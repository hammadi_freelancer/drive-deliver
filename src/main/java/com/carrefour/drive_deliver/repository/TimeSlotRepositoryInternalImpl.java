package com.carrefour.drive_deliver.repository;

import com.carrefour.drive_deliver.domain.TimeSlot;
import com.carrefour.drive_deliver.repository.rowmapper.DeliveryMethodRowMapper;
import com.carrefour.drive_deliver.repository.rowmapper.TimeSlotRowMapper;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.util.List;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.convert.R2dbcConverter;
import org.springframework.data.r2dbc.core.R2dbcEntityOperations;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.r2dbc.repository.support.SimpleR2dbcRepository;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Comparison;
import org.springframework.data.relational.core.sql.Condition;
import org.springframework.data.relational.core.sql.Conditions;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoinCondition;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.data.relational.repository.support.MappingRelationalEntityInformation;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data R2DBC custom repository implementation for the TimeSlot entity.
 */
@SuppressWarnings("unused")
class TimeSlotRepositoryInternalImpl extends SimpleR2dbcRepository<TimeSlot, UUID> implements TimeSlotRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final DeliveryMethodRowMapper deliverymethodMapper;
    private final TimeSlotRowMapper timeslotMapper;

    private static final Table entityTable = Table.aliased("time_slot", EntityManager.ENTITY_ALIAS);
    private static final Table deliveryMethodTable = Table.aliased("delivery_method", "deliveryMethod");

    public TimeSlotRepositoryInternalImpl(
        R2dbcEntityTemplate template,
        EntityManager entityManager,
        DeliveryMethodRowMapper deliverymethodMapper,
        TimeSlotRowMapper timeslotMapper,
        R2dbcEntityOperations entityOperations,
        R2dbcConverter converter
    ) {
        super(
            new MappingRelationalEntityInformation(converter.getMappingContext().getRequiredPersistentEntity(TimeSlot.class)),
            entityOperations,
            converter
        );
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.deliverymethodMapper = deliverymethodMapper;
        this.timeslotMapper = timeslotMapper;
    }

    @Override
    public Flux<TimeSlot> findAllBy(Pageable pageable) {
        return createQuery(pageable, null).all();
    }

    RowsFetchSpec<TimeSlot> createQuery(Pageable pageable, Condition whereClause) {
        List<Expression> columns = TimeSlotSqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        columns.addAll(DeliveryMethodSqlHelper.getColumns(deliveryMethodTable, "deliveryMethod"));
        SelectFromAndJoinCondition selectFrom = Select.builder()
            .select(columns)
            .from(entityTable)
            .leftOuterJoin(deliveryMethodTable)
            .on(Column.create("delivery_method_id", entityTable))
            .equals(Column.create("id", deliveryMethodTable));
        // we do not support Criteria here for now as of https://github.com/jhipster/generator-jhipster/issues/18269
        String select = entityManager.createSelect(selectFrom, TimeSlot.class, pageable, whereClause);
        return db.sql(select).map(this::process);
    }

    @Override
    public Flux<TimeSlot> findAll() {
        return findAllBy(null);
    }

    @Override
    public Mono<TimeSlot> findById(UUID id) {
        Comparison whereClause = Conditions.isEqual(entityTable.column("id"), Conditions.just(StringUtils.wrap(id.toString(), "'")));
        return createQuery(null, whereClause).one();
    }

    private TimeSlot process(Row row, RowMetadata metadata) {
        TimeSlot entity = timeslotMapper.apply(row, "e");
        entity.setDeliveryMethod(deliverymethodMapper.apply(row, "deliveryMethod"));
        return entity;
    }

    @Override
    public <S extends TimeSlot> Mono<S> save(S entity) {
        return super.save(entity);
    }
}
