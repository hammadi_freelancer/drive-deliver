package com.carrefour.drive_deliver.repository.rowmapper;

import com.carrefour.drive_deliver.domain.Evenement;
import io.r2dbc.spi.Row;
import java.util.UUID;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Evenement}, with proper type conversions.
 */
@Service
public class EvenementRowMapper implements BiFunction<Row, String, Evenement> {

    private final ColumnConverter converter;

    public EvenementRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Evenement} stored in the database.
     */
    @Override
    public Evenement apply(Row row, String prefix) {
        Evenement entity = new Evenement();
        entity.setId(converter.fromRow(row, prefix + "_id", UUID.class));
        entity.setLibelle(converter.fromRow(row, prefix + "_libelle", String.class));
        return entity;
    }
}
