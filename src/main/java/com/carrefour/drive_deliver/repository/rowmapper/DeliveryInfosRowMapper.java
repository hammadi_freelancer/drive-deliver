package com.carrefour.drive_deliver.repository.rowmapper;

import com.carrefour.drive_deliver.domain.DeliveryInfos;
import io.r2dbc.spi.Row;
import java.time.LocalDate;
import java.util.UUID;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link DeliveryInfos}, with proper type conversions.
 */
@Service
public class DeliveryInfosRowMapper implements BiFunction<Row, String, DeliveryInfos> {

    private final ColumnConverter converter;

    public DeliveryInfosRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link DeliveryInfos} stored in the database.
     */
    @Override
    public DeliveryInfos apply(Row row, String prefix) {
        DeliveryInfos entity = new DeliveryInfos();
        entity.setId(converter.fromRow(row, prefix + "_id", UUID.class));
        entity.setDeliveryDate(converter.fromRow(row, prefix + "_delivery_date", LocalDate.class));
        entity.setDescription(converter.fromRow(row, prefix + "_description", String.class));
        entity.setDeliveryMethodId(converter.fromRow(row, prefix + "_delivery_method_id", UUID.class));
        entity.setSlotId(converter.fromRow(row, prefix + "_slot_id", UUID.class));
        return entity;
    }
}
