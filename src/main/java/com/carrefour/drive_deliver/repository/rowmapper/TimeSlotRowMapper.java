package com.carrefour.drive_deliver.repository.rowmapper;

import com.carrefour.drive_deliver.domain.TimeSlot;
import io.r2dbc.spi.Row;
import java.time.Instant;
import java.util.UUID;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link TimeSlot}, with proper type conversions.
 */
@Service
public class TimeSlotRowMapper implements BiFunction<Row, String, TimeSlot> {

    private final ColumnConverter converter;

    public TimeSlotRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link TimeSlot} stored in the database.
     */
    @Override
    public TimeSlot apply(Row row, String prefix) {
        TimeSlot entity = new TimeSlot();
        entity.setId(converter.fromRow(row, prefix + "_id", UUID.class));
        entity.setStartHour(converter.fromRow(row, prefix + "_start_hour", Instant.class));
        entity.setEndHour(converter.fromRow(row, prefix + "_end_hour", Instant.class));
        entity.setDeliveryMethodId(converter.fromRow(row, prefix + "_delivery_method_id", UUID.class));
        return entity;
    }
}
