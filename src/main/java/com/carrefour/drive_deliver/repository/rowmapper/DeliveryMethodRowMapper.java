package com.carrefour.drive_deliver.repository.rowmapper;

import com.carrefour.drive_deliver.domain.DeliveryMethod;
import com.carrefour.drive_deliver.domain.enumeration.LibelleDeliveryMethod;
import io.r2dbc.spi.Row;
import java.util.UUID;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link DeliveryMethod}, with proper type conversions.
 */
@Service
public class DeliveryMethodRowMapper implements BiFunction<Row, String, DeliveryMethod> {

    private final ColumnConverter converter;

    public DeliveryMethodRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link DeliveryMethod} stored in the database.
     */
    @Override
    public DeliveryMethod apply(Row row, String prefix) {
        DeliveryMethod entity = new DeliveryMethod();
        entity.setId(converter.fromRow(row, prefix + "_id", UUID.class));
        entity.setLibelle(converter.fromRow(row, prefix + "_libelle", LibelleDeliveryMethod.class));
        entity.setDescription(converter.fromRow(row, prefix + "_description", String.class));
        return entity;
    }
}
