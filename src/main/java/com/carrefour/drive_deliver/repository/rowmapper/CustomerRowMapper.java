package com.carrefour.drive_deliver.repository.rowmapper;

import com.carrefour.drive_deliver.domain.Customer;
import io.r2dbc.spi.Row;
import java.util.UUID;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Customer}, with proper type conversions.
 */
@Service
public class CustomerRowMapper implements BiFunction<Row, String, Customer> {

    private final ColumnConverter converter;

    public CustomerRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Customer} stored in the database.
     */
    @Override
    public Customer apply(Row row, String prefix) {
        Customer entity = new Customer();
        entity.setId(converter.fromRow(row, prefix + "_id", UUID.class));
        entity.setCin(converter.fromRow(row, prefix + "_cin", String.class));
        entity.setNom(converter.fromRow(row, prefix + "_nom", String.class));
        entity.setPrenom(converter.fromRow(row, prefix + "_prenom", String.class));
        entity.setAdresse(converter.fromRow(row, prefix + "_adresse", String.class));
        entity.setTel(converter.fromRow(row, prefix + "_tel", String.class));
        entity.setEmail(converter.fromRow(row, prefix + "_email", String.class));
        entity.setDeliveryInfosId(converter.fromRow(row, prefix + "_delivery_infos_id", UUID.class));
        return entity;
    }
}
