package com.carrefour.drive_deliver.repository;

import com.carrefour.drive_deliver.domain.DeliveryInfos;
import com.carrefour.drive_deliver.repository.rowmapper.DeliveryInfosRowMapper;
import com.carrefour.drive_deliver.repository.rowmapper.DeliveryMethodRowMapper;
import com.carrefour.drive_deliver.repository.rowmapper.TimeSlotRowMapper;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.util.List;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.convert.R2dbcConverter;
import org.springframework.data.r2dbc.core.R2dbcEntityOperations;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.r2dbc.repository.support.SimpleR2dbcRepository;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Comparison;
import org.springframework.data.relational.core.sql.Condition;
import org.springframework.data.relational.core.sql.Conditions;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoinCondition;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.data.relational.repository.support.MappingRelationalEntityInformation;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data R2DBC custom repository implementation for the DeliveryInfos entity.
 */
@SuppressWarnings("unused")
class DeliveryInfosRepositoryInternalImpl extends SimpleR2dbcRepository<DeliveryInfos, UUID> implements DeliveryInfosRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final DeliveryMethodRowMapper deliverymethodMapper;
    private final TimeSlotRowMapper timeslotMapper;
    private final DeliveryInfosRowMapper deliveryinfosMapper;

    private static final Table entityTable = Table.aliased("delivery_infos", EntityManager.ENTITY_ALIAS);
    private static final Table deliveryMethodTable = Table.aliased("delivery_method", "deliveryMethod");
    private static final Table slotTable = Table.aliased("time_slot", "slot");

    public DeliveryInfosRepositoryInternalImpl(
        R2dbcEntityTemplate template,
        EntityManager entityManager,
        DeliveryMethodRowMapper deliverymethodMapper,
        TimeSlotRowMapper timeslotMapper,
        DeliveryInfosRowMapper deliveryinfosMapper,
        R2dbcEntityOperations entityOperations,
        R2dbcConverter converter
    ) {
        super(
            new MappingRelationalEntityInformation(converter.getMappingContext().getRequiredPersistentEntity(DeliveryInfos.class)),
            entityOperations,
            converter
        );
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.deliverymethodMapper = deliverymethodMapper;
        this.timeslotMapper = timeslotMapper;
        this.deliveryinfosMapper = deliveryinfosMapper;
    }

    @Override
    public Flux<DeliveryInfos> findAllBy(Pageable pageable) {
        return createQuery(pageable, null).all();
    }

    RowsFetchSpec<DeliveryInfos> createQuery(Pageable pageable, Condition whereClause) {
        List<Expression> columns = DeliveryInfosSqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        columns.addAll(DeliveryMethodSqlHelper.getColumns(deliveryMethodTable, "deliveryMethod"));
        columns.addAll(TimeSlotSqlHelper.getColumns(slotTable, "slot"));
        SelectFromAndJoinCondition selectFrom = Select.builder()
            .select(columns)
            .from(entityTable)
            .leftOuterJoin(deliveryMethodTable)
            .on(Column.create("delivery_method_id", entityTable))
            .equals(Column.create("id", deliveryMethodTable))
            .leftOuterJoin(slotTable)
            .on(Column.create("slot_id", entityTable))
            .equals(Column.create("id", slotTable));
        // we do not support Criteria here for now as of https://github.com/jhipster/generator-jhipster/issues/18269
        String select = entityManager.createSelect(selectFrom, DeliveryInfos.class, pageable, whereClause);
        return db.sql(select).map(this::process);
    }

    @Override
    public Flux<DeliveryInfos> findAll() {
        return findAllBy(null);
    }

    @Override
    public Mono<DeliveryInfos> findById(UUID id) {
        Comparison whereClause = Conditions.isEqual(entityTable.column("id"), Conditions.just(StringUtils.wrap(id.toString(), "'")));
        return createQuery(null, whereClause).one();
    }

    private DeliveryInfos process(Row row, RowMetadata metadata) {
        DeliveryInfos entity = deliveryinfosMapper.apply(row, "e");
        entity.setDeliveryMethod(deliverymethodMapper.apply(row, "deliveryMethod"));
        entity.setSlot(timeslotMapper.apply(row, "slot"));
        return entity;
    }

    @Override
    public <S extends DeliveryInfos> Mono<S> save(S entity) {
        return super.save(entity);
    }
}
