package com.carrefour.drive_deliver.repository;

import com.carrefour.drive_deliver.domain.DeliveryMethod;
import java.util.UUID;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data R2DBC repository for the DeliveryMethod entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DeliveryMethodRepository extends ReactiveCrudRepository<DeliveryMethod, UUID>, DeliveryMethodRepositoryInternal {
    Flux<DeliveryMethod> findAllBy(Pageable pageable);

    @Override
    <S extends DeliveryMethod> Mono<S> save(S entity);

    @Override
    Flux<DeliveryMethod> findAll();

    @Override
    Mono<DeliveryMethod> findById(UUID id);

    @Override
    Mono<Void> deleteById(UUID id);
}

interface DeliveryMethodRepositoryInternal {
    <S extends DeliveryMethod> Mono<S> save(S entity);

    Flux<DeliveryMethod> findAllBy(Pageable pageable);

    Flux<DeliveryMethod> findAll();

    Mono<DeliveryMethod> findById(UUID id);
    // this is not supported at the moment because of https://github.com/jhipster/generator-jhipster/issues/18269
    // Flux<DeliveryMethod> findAllBy(Pageable pageable, Criteria criteria);
}
