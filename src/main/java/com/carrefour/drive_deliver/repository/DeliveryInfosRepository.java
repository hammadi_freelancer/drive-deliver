package com.carrefour.drive_deliver.repository;

import com.carrefour.drive_deliver.domain.DeliveryInfos;
import java.util.UUID;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data R2DBC repository for the DeliveryInfos entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DeliveryInfosRepository extends ReactiveCrudRepository<DeliveryInfos, UUID>, DeliveryInfosRepositoryInternal {
    Flux<DeliveryInfos> findAllBy(Pageable pageable);

    @Query("SELECT * FROM delivery_infos entity WHERE entity.id not in (select customer_id from customer)")
    Flux<DeliveryInfos> findAllWhereCustomerIsNull();

    @Query("SELECT * FROM delivery_infos entity WHERE entity.delivery_method_id = :id")
    Flux<DeliveryInfos> findByDeliveryMethod(UUID id);

    @Query("SELECT * FROM delivery_infos entity WHERE entity.delivery_method_id IS NULL")
    Flux<DeliveryInfos> findAllWhereDeliveryMethodIsNull();

    @Query("SELECT * FROM delivery_infos entity WHERE entity.slot_id = :id")
    Flux<DeliveryInfos> findBySlot(UUID id);

    @Query("SELECT * FROM delivery_infos entity WHERE entity.slot_id IS NULL")
    Flux<DeliveryInfos> findAllWhereSlotIsNull();

    @Override
    <S extends DeliveryInfos> Mono<S> save(S entity);

    @Override
    Flux<DeliveryInfos> findAll();

    @Override
    Mono<DeliveryInfos> findById(UUID id);

    @Override
    Mono<Void> deleteById(UUID id);
}

interface DeliveryInfosRepositoryInternal {
    <S extends DeliveryInfos> Mono<S> save(S entity);

    Flux<DeliveryInfos> findAllBy(Pageable pageable);

    Flux<DeliveryInfos> findAll();

    Mono<DeliveryInfos> findById(UUID id);
    // this is not supported at the moment because of https://github.com/jhipster/generator-jhipster/issues/18269
    // Flux<DeliveryInfos> findAllBy(Pageable pageable, Criteria criteria);
}
