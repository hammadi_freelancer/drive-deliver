package com.carrefour.drive_deliver.repository;

import com.carrefour.drive_deliver.domain.TimeSlot;
import java.util.UUID;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data R2DBC repository for the TimeSlot entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TimeSlotRepository extends ReactiveCrudRepository<TimeSlot, UUID>, TimeSlotRepositoryInternal {
    Flux<TimeSlot> findAllBy(Pageable pageable);

    @Query("SELECT * FROM time_slot entity WHERE entity.delivery_method_id = :id")
    Flux<TimeSlot> findByDeliveryMethod(UUID id);

    @Query("SELECT * FROM time_slot entity WHERE entity.delivery_method_id IS NULL")
    Flux<TimeSlot> findAllWhereDeliveryMethodIsNull();

    @Override
    <S extends TimeSlot> Mono<S> save(S entity);

    @Override
    Flux<TimeSlot> findAll();

    @Override
    Mono<TimeSlot> findById(UUID id);

    @Override
    Mono<Void> deleteById(UUID id);
}

interface TimeSlotRepositoryInternal {
    <S extends TimeSlot> Mono<S> save(S entity);

    Flux<TimeSlot> findAllBy(Pageable pageable);

    Flux<TimeSlot> findAll();

    Mono<TimeSlot> findById(UUID id);
    // this is not supported at the moment because of https://github.com/jhipster/generator-jhipster/issues/18269
    // Flux<TimeSlot> findAllBy(Pageable pageable, Criteria criteria);
}
