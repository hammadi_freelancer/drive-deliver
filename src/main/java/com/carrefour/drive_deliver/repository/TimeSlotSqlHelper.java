package com.carrefour.drive_deliver.repository;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Table;

public class TimeSlotSqlHelper {

    public static List<Expression> getColumns(Table table, String columnPrefix) {
        List<Expression> columns = new ArrayList<>();
        columns.add(Column.aliased("id", table, columnPrefix + "_id"));
        columns.add(Column.aliased("start_hour", table, columnPrefix + "_start_hour"));
        columns.add(Column.aliased("end_hour", table, columnPrefix + "_end_hour"));

        columns.add(Column.aliased("delivery_method_id", table, columnPrefix + "_delivery_method_id"));
        return columns;
    }
}
