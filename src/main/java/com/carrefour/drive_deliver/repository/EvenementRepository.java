package com.carrefour.drive_deliver.repository;

import com.carrefour.drive_deliver.domain.Evenement;
import java.util.UUID;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data R2DBC repository for the Evenement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EvenementRepository extends ReactiveCrudRepository<Evenement, UUID>, EvenementRepositoryInternal {
    Flux<Evenement> findAllBy(Pageable pageable);

    @Override
    <S extends Evenement> Mono<S> save(S entity);

    @Override
    Flux<Evenement> findAll();

    @Override
    Mono<Evenement> findById(UUID id);

    @Override
    Mono<Void> deleteById(UUID id);
}

interface EvenementRepositoryInternal {
    <S extends Evenement> Mono<S> save(S entity);

    Flux<Evenement> findAllBy(Pageable pageable);

    Flux<Evenement> findAll();

    Mono<Evenement> findById(UUID id);
    // this is not supported at the moment because of https://github.com/jhipster/generator-jhipster/issues/18269
    // Flux<Evenement> findAllBy(Pageable pageable, Criteria criteria);
}
