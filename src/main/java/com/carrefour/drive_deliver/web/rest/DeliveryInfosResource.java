package com.carrefour.drive_deliver.web.rest;

import com.carrefour.drive_deliver.repository.DeliveryInfosRepository;
import com.carrefour.drive_deliver.service.DeliveryInfosService;
import com.carrefour.drive_deliver.service.dto.DeliveryInfosDTO;
import com.carrefour.drive_deliver.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.ForwardedHeaderUtils;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.carrefour.drive_deliver.domain.DeliveryInfos}.
 */
@RestController
@RequestMapping("/api/delivery-infos")
public class DeliveryInfosResource {

    private static final Logger log = LoggerFactory.getLogger(DeliveryInfosResource.class);

    private static final String ENTITY_NAME = "deliveryInfos";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DeliveryInfosService deliveryInfosService;

    private final DeliveryInfosRepository deliveryInfosRepository;

    public DeliveryInfosResource(DeliveryInfosService deliveryInfosService, DeliveryInfosRepository deliveryInfosRepository) {
        this.deliveryInfosService = deliveryInfosService;
        this.deliveryInfosRepository = deliveryInfosRepository;
    }

    /**
     * {@code POST  /delivery-infos} : Create a new deliveryInfos.
     *
     * @param deliveryInfosDTO the deliveryInfosDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new deliveryInfosDTO, or with status {@code 400 (Bad Request)} if the deliveryInfos has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public Mono<ResponseEntity<DeliveryInfosDTO>> createDeliveryInfos(@RequestBody DeliveryInfosDTO deliveryInfosDTO)
        throws URISyntaxException {
        log.debug("REST request to save DeliveryInfos : {}", deliveryInfosDTO);
        if (deliveryInfosDTO.getId() != null) {
            throw new BadRequestAlertException("A new deliveryInfos cannot already have an ID", ENTITY_NAME, "idexists");
        }
        deliveryInfosDTO.setId(UUID.randomUUID());
        return deliveryInfosService
            .save(deliveryInfosDTO)
            .map(result -> {
                try {
                    return ResponseEntity.created(new URI("/api/delivery-infos/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /delivery-infos/:id} : Updates an existing deliveryInfos.
     *
     * @param id the id of the deliveryInfosDTO to save.
     * @param deliveryInfosDTO the deliveryInfosDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated deliveryInfosDTO,
     * or with status {@code 400 (Bad Request)} if the deliveryInfosDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the deliveryInfosDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public Mono<ResponseEntity<DeliveryInfosDTO>> updateDeliveryInfos(
        @PathVariable(value = "id", required = false) final UUID id,
        @RequestBody DeliveryInfosDTO deliveryInfosDTO
    ) throws URISyntaxException {
        log.debug("REST request to update DeliveryInfos : {}, {}", id, deliveryInfosDTO);
        if (deliveryInfosDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, deliveryInfosDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return deliveryInfosRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                return deliveryInfosService
                    .update(deliveryInfosDTO)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(
                        result ->
                            ResponseEntity.ok()
                                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
                                .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /delivery-infos/:id} : Partial updates given fields of an existing deliveryInfos, field will ignore if it is null
     *
     * @param id the id of the deliveryInfosDTO to save.
     * @param deliveryInfosDTO the deliveryInfosDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated deliveryInfosDTO,
     * or with status {@code 400 (Bad Request)} if the deliveryInfosDTO is not valid,
     * or with status {@code 404 (Not Found)} if the deliveryInfosDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the deliveryInfosDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<DeliveryInfosDTO>> partialUpdateDeliveryInfos(
        @PathVariable(value = "id", required = false) final UUID id,
        @RequestBody DeliveryInfosDTO deliveryInfosDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update DeliveryInfos partially : {}, {}", id, deliveryInfosDTO);
        if (deliveryInfosDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, deliveryInfosDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return deliveryInfosRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                Mono<DeliveryInfosDTO> result = deliveryInfosService.partialUpdate(deliveryInfosDTO);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(
                        res ->
                            ResponseEntity.ok()
                                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, res.getId().toString()))
                                .body(res)
                    );
            });
    }

    /**
     * {@code GET  /delivery-infos} : get all the deliveryInfos.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @param filter the filter of the request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of deliveryInfos in body.
     */
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<List<DeliveryInfosDTO>>> getAllDeliveryInfos(
        @org.springdoc.core.annotations.ParameterObject Pageable pageable,
        ServerHttpRequest request,
        @RequestParam(name = "filter", required = false) String filter
    ) {
        if ("customer-is-null".equals(filter)) {
            log.debug("REST request to get all DeliveryInfoss where customer is null");
            return deliveryInfosService.findAllWhereCustomerIsNull().collectList().map(ResponseEntity::ok);
        }
        log.debug("REST request to get a page of DeliveryInfos");
        return deliveryInfosService
            .countAll()
            .zipWith(deliveryInfosService.findAll(pageable).collectList())
            .map(
                countWithEntities ->
                    ResponseEntity.ok()
                        .headers(
                            PaginationUtil.generatePaginationHttpHeaders(
                                ForwardedHeaderUtils.adaptFromForwardedHeaders(request.getURI(), request.getHeaders()),
                                new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                            )
                        )
                        .body(countWithEntities.getT2())
            );
    }

    /**
     * {@code GET  /delivery-infos/:id} : get the "id" deliveryInfos.
     *
     * @param id the id of the deliveryInfosDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the deliveryInfosDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public Mono<ResponseEntity<DeliveryInfosDTO>> getDeliveryInfos(@PathVariable("id") UUID id) {
        log.debug("REST request to get DeliveryInfos : {}", id);
        Mono<DeliveryInfosDTO> deliveryInfosDTO = deliveryInfosService.findOne(id);
        return ResponseUtil.wrapOrNotFound(deliveryInfosDTO);
    }

    /**
     * {@code DELETE  /delivery-infos/:id} : delete the "id" deliveryInfos.
     *
     * @param id the id of the deliveryInfosDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> deleteDeliveryInfos(@PathVariable("id") UUID id) {
        log.debug("REST request to delete DeliveryInfos : {}", id);
        return deliveryInfosService
            .delete(id)
            .then(
                Mono.just(
                    ResponseEntity.noContent()
                        .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
                        .build()
                )
            );
    }
}
