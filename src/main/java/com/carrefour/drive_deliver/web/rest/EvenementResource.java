package com.carrefour.drive_deliver.web.rest;

import com.carrefour.drive_deliver.repository.EvenementRepository;
import com.carrefour.drive_deliver.service.EvenementService;
import com.carrefour.drive_deliver.service.dto.EvenementDTO;
import com.carrefour.drive_deliver.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.ForwardedHeaderUtils;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.carrefour.drive_deliver.domain.Evenement}.
 */
@RestController
@RequestMapping("/api/evenements")
public class EvenementResource {

    private static final Logger log = LoggerFactory.getLogger(EvenementResource.class);

    private static final String ENTITY_NAME = "evenement";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EvenementService evenementService;

    private final EvenementRepository evenementRepository;

    public EvenementResource(EvenementService evenementService, EvenementRepository evenementRepository) {
        this.evenementService = evenementService;
        this.evenementRepository = evenementRepository;
    }

    /**
     * {@code POST  /evenements} : Create a new evenement.
     *
     * @param evenementDTO the evenementDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new evenementDTO, or with status {@code 400 (Bad Request)} if the evenement has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public Mono<ResponseEntity<EvenementDTO>> createEvenement(@RequestBody EvenementDTO evenementDTO) throws URISyntaxException {
        log.debug("REST request to save Evenement : {}", evenementDTO);
        if (evenementDTO.getId() != null) {
            throw new BadRequestAlertException("A new evenement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        evenementDTO.setId(UUID.randomUUID());
        return evenementService
            .save(evenementDTO)
            .map(result -> {
                try {
                    return ResponseEntity.created(new URI("/api/evenements/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /evenements/:id} : Updates an existing evenement.
     *
     * @param id the id of the evenementDTO to save.
     * @param evenementDTO the evenementDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated evenementDTO,
     * or with status {@code 400 (Bad Request)} if the evenementDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the evenementDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public Mono<ResponseEntity<EvenementDTO>> updateEvenement(
        @PathVariable(value = "id", required = false) final UUID id,
        @RequestBody EvenementDTO evenementDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Evenement : {}, {}", id, evenementDTO);
        if (evenementDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, evenementDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return evenementRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                return evenementService
                    .update(evenementDTO)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(
                        result ->
                            ResponseEntity.ok()
                                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
                                .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /evenements/:id} : Partial updates given fields of an existing evenement, field will ignore if it is null
     *
     * @param id the id of the evenementDTO to save.
     * @param evenementDTO the evenementDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated evenementDTO,
     * or with status {@code 400 (Bad Request)} if the evenementDTO is not valid,
     * or with status {@code 404 (Not Found)} if the evenementDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the evenementDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<EvenementDTO>> partialUpdateEvenement(
        @PathVariable(value = "id", required = false) final UUID id,
        @RequestBody EvenementDTO evenementDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Evenement partially : {}, {}", id, evenementDTO);
        if (evenementDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, evenementDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return evenementRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                Mono<EvenementDTO> result = evenementService.partialUpdate(evenementDTO);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(
                        res ->
                            ResponseEntity.ok()
                                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, res.getId().toString()))
                                .body(res)
                    );
            });
    }

    /**
     * {@code GET  /evenements} : get all the evenements.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of evenements in body.
     */
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<List<EvenementDTO>>> getAllEvenements(
        @org.springdoc.core.annotations.ParameterObject Pageable pageable,
        ServerHttpRequest request
    ) {
        log.debug("REST request to get a page of Evenements");
        return evenementService
            .countAll()
            .zipWith(evenementService.findAll(pageable).collectList())
            .map(
                countWithEntities ->
                    ResponseEntity.ok()
                        .headers(
                            PaginationUtil.generatePaginationHttpHeaders(
                                ForwardedHeaderUtils.adaptFromForwardedHeaders(request.getURI(), request.getHeaders()),
                                new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                            )
                        )
                        .body(countWithEntities.getT2())
            );
    }

    /**
     * {@code GET  /evenements/:id} : get the "id" evenement.
     *
     * @param id the id of the evenementDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the evenementDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public Mono<ResponseEntity<EvenementDTO>> getEvenement(@PathVariable("id") UUID id) {
        log.debug("REST request to get Evenement : {}", id);
        Mono<EvenementDTO> evenementDTO = evenementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(evenementDTO);
    }

    /**
     * {@code DELETE  /evenements/:id} : delete the "id" evenement.
     *
     * @param id the id of the evenementDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> deleteEvenement(@PathVariable("id") UUID id) {
        log.debug("REST request to delete Evenement : {}", id);
        return evenementService
            .delete(id)
            .then(
                Mono.just(
                    ResponseEntity.noContent()
                        .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
                        .build()
                )
            );
    }
}
