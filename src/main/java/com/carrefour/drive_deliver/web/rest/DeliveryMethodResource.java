package com.carrefour.drive_deliver.web.rest;

import com.carrefour.drive_deliver.repository.DeliveryMethodRepository;
import com.carrefour.drive_deliver.service.DeliveryMethodService;
import com.carrefour.drive_deliver.service.dto.DeliveryMethodDTO;
import com.carrefour.drive_deliver.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.ForwardedHeaderUtils;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.carrefour.drive_deliver.domain.DeliveryMethod}.
 */
@RestController
@RequestMapping("/api/delivery-methods")
public class DeliveryMethodResource {

    private static final Logger log = LoggerFactory.getLogger(DeliveryMethodResource.class);

    private static final String ENTITY_NAME = "deliveryMethod";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DeliveryMethodService deliveryMethodService;

    private final DeliveryMethodRepository deliveryMethodRepository;

    public DeliveryMethodResource(DeliveryMethodService deliveryMethodService, DeliveryMethodRepository deliveryMethodRepository) {
        this.deliveryMethodService = deliveryMethodService;
        this.deliveryMethodRepository = deliveryMethodRepository;
    }

    /**
     * {@code POST  /delivery-methods} : Create a new deliveryMethod.
     *
     * @param deliveryMethodDTO the deliveryMethodDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new deliveryMethodDTO, or with status {@code 400 (Bad Request)} if the deliveryMethod has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public Mono<ResponseEntity<DeliveryMethodDTO>> createDeliveryMethod(@RequestBody DeliveryMethodDTO deliveryMethodDTO)
        throws URISyntaxException {
        log.debug("REST request to save DeliveryMethod : {}", deliveryMethodDTO);
        if (deliveryMethodDTO.getId() != null) {
            throw new BadRequestAlertException("A new deliveryMethod cannot already have an ID", ENTITY_NAME, "idexists");
        }
        deliveryMethodDTO.setId(UUID.randomUUID());
        return deliveryMethodService
            .save(deliveryMethodDTO)
            .map(result -> {
                try {
                    return ResponseEntity.created(new URI("/api/delivery-methods/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /delivery-methods/:id} : Updates an existing deliveryMethod.
     *
     * @param id the id of the deliveryMethodDTO to save.
     * @param deliveryMethodDTO the deliveryMethodDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated deliveryMethodDTO,
     * or with status {@code 400 (Bad Request)} if the deliveryMethodDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the deliveryMethodDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public Mono<ResponseEntity<DeliveryMethodDTO>> updateDeliveryMethod(
        @PathVariable(value = "id", required = false) final UUID id,
        @RequestBody DeliveryMethodDTO deliveryMethodDTO
    ) throws URISyntaxException {
        log.debug("REST request to update DeliveryMethod : {}, {}", id, deliveryMethodDTO);
        if (deliveryMethodDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, deliveryMethodDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return deliveryMethodRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                return deliveryMethodService
                    .update(deliveryMethodDTO)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(
                        result ->
                            ResponseEntity.ok()
                                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
                                .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /delivery-methods/:id} : Partial updates given fields of an existing deliveryMethod, field will ignore if it is null
     *
     * @param id the id of the deliveryMethodDTO to save.
     * @param deliveryMethodDTO the deliveryMethodDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated deliveryMethodDTO,
     * or with status {@code 400 (Bad Request)} if the deliveryMethodDTO is not valid,
     * or with status {@code 404 (Not Found)} if the deliveryMethodDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the deliveryMethodDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<DeliveryMethodDTO>> partialUpdateDeliveryMethod(
        @PathVariable(value = "id", required = false) final UUID id,
        @RequestBody DeliveryMethodDTO deliveryMethodDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update DeliveryMethod partially : {}, {}", id, deliveryMethodDTO);
        if (deliveryMethodDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, deliveryMethodDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return deliveryMethodRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                Mono<DeliveryMethodDTO> result = deliveryMethodService.partialUpdate(deliveryMethodDTO);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(
                        res ->
                            ResponseEntity.ok()
                                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, res.getId().toString()))
                                .body(res)
                    );
            });
    }

    /**
     * {@code GET  /delivery-methods} : get all the deliveryMethods.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of deliveryMethods in body.
     */
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<List<DeliveryMethodDTO>>> getAllDeliveryMethods(
        @org.springdoc.core.annotations.ParameterObject Pageable pageable,
        ServerHttpRequest request
    ) {
        log.debug("REST request to get a page of DeliveryMethods");
        return deliveryMethodService
            .countAll()
            .zipWith(deliveryMethodService.findAll(pageable).collectList())
            .map(
                countWithEntities ->
                    ResponseEntity.ok()
                        .headers(
                            PaginationUtil.generatePaginationHttpHeaders(
                                ForwardedHeaderUtils.adaptFromForwardedHeaders(request.getURI(), request.getHeaders()),
                                new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                            )
                        )
                        .body(countWithEntities.getT2())
            );
    }

    /**
     * {@code GET  /delivery-methods/:id} : get the "id" deliveryMethod.
     *
     * @param id the id of the deliveryMethodDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the deliveryMethodDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public Mono<ResponseEntity<DeliveryMethodDTO>> getDeliveryMethod(@PathVariable("id") UUID id) {
        log.debug("REST request to get DeliveryMethod : {}", id);
        Mono<DeliveryMethodDTO> deliveryMethodDTO = deliveryMethodService.findOne(id);
        return ResponseUtil.wrapOrNotFound(deliveryMethodDTO);
    }

    /**
     * {@code DELETE  /delivery-methods/:id} : delete the "id" deliveryMethod.
     *
     * @param id the id of the deliveryMethodDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> deleteDeliveryMethod(@PathVariable("id") UUID id) {
        log.debug("REST request to delete DeliveryMethod : {}", id);
        return deliveryMethodService
            .delete(id)
            .then(
                Mono.just(
                    ResponseEntity.noContent()
                        .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
                        .build()
                )
            );
    }
}
