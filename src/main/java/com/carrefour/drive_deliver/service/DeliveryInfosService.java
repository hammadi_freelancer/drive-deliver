package com.carrefour.drive_deliver.service;

import com.carrefour.drive_deliver.service.dto.DeliveryInfosDTO;
import java.util.UUID;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Interface for managing {@link com.carrefour.drive_deliver.domain.DeliveryInfos}.
 */
public interface DeliveryInfosService {
    /**
     * Save a deliveryInfos.
     *
     * @param deliveryInfosDTO the entity to save.
     * @return the persisted entity.
     */
    Mono<DeliveryInfosDTO> save(DeliveryInfosDTO deliveryInfosDTO);

    /**
     * Updates a deliveryInfos.
     *
     * @param deliveryInfosDTO the entity to update.
     * @return the persisted entity.
     */
    Mono<DeliveryInfosDTO> update(DeliveryInfosDTO deliveryInfosDTO);

    /**
     * Partially updates a deliveryInfos.
     *
     * @param deliveryInfosDTO the entity to update partially.
     * @return the persisted entity.
     */
    Mono<DeliveryInfosDTO> partialUpdate(DeliveryInfosDTO deliveryInfosDTO);

    /**
     * Get all the deliveryInfos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Flux<DeliveryInfosDTO> findAll(Pageable pageable);

    /**
     * Get all the DeliveryInfosDTO where Customer is {@code null}.
     *
     * @return the {@link Flux} of entities.
     */
    Flux<DeliveryInfosDTO> findAllWhereCustomerIsNull();

    /**
     * Returns the number of deliveryInfos available.
     * @return the number of entities in the database.
     *
     */
    Mono<Long> countAll();

    /**
     * Get the "id" deliveryInfos.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Mono<DeliveryInfosDTO> findOne(UUID id);

    /**
     * Delete the "id" deliveryInfos.
     *
     * @param id the id of the entity.
     * @return a Mono to signal the deletion
     */
    Mono<Void> delete(UUID id);
}
