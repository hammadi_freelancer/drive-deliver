package com.carrefour.drive_deliver.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the {@link com.carrefour.drive_deliver.domain.DeliveryInfos} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DeliveryInfosDTO implements Serializable {

    private UUID id;

    private LocalDate deliveryDate;

    private String description;

    private DeliveryMethodDTO deliveryMethod;

    private TimeSlotDTO slot;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public LocalDate getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(LocalDate deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DeliveryMethodDTO getDeliveryMethod() {
        return deliveryMethod;
    }

    public void setDeliveryMethod(DeliveryMethodDTO deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    public TimeSlotDTO getSlot() {
        return slot;
    }

    public void setSlot(TimeSlotDTO slot) {
        this.slot = slot;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DeliveryInfosDTO)) {
            return false;
        }

        DeliveryInfosDTO deliveryInfosDTO = (DeliveryInfosDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, deliveryInfosDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DeliveryInfosDTO{" +
            "id='" + getId() + "'" +
            ", deliveryDate='" + getDeliveryDate() + "'" +
            ", description='" + getDescription() + "'" +
            ", deliveryMethod=" + getDeliveryMethod() +
            ", slot=" + getSlot() +
            "}";
    }
}
