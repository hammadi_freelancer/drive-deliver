package com.carrefour.drive_deliver.service.dto;

import com.carrefour.drive_deliver.domain.enumeration.LibelleDeliveryMethod;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the {@link com.carrefour.drive_deliver.domain.DeliveryMethod} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DeliveryMethodDTO implements Serializable {

    private UUID id;

    private LibelleDeliveryMethod libelle;

    private String description;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public LibelleDeliveryMethod getLibelle() {
        return libelle;
    }

    public void setLibelle(LibelleDeliveryMethod libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DeliveryMethodDTO)) {
            return false;
        }

        DeliveryMethodDTO deliveryMethodDTO = (DeliveryMethodDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, deliveryMethodDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DeliveryMethodDTO{" +
            "id='" + getId() + "'" +
            ", libelle='" + getLibelle() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
