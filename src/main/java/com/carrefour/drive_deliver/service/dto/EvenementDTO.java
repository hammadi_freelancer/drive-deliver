package com.carrefour.drive_deliver.service.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the {@link com.carrefour.drive_deliver.domain.Evenement} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class EvenementDTO implements Serializable {

    private UUID id;

    private String libelle;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EvenementDTO)) {
            return false;
        }

        EvenementDTO evenementDTO = (EvenementDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, evenementDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EvenementDTO{" +
            "id='" + getId() + "'" +
            ", libelle='" + getLibelle() + "'" +
            "}";
    }
}
