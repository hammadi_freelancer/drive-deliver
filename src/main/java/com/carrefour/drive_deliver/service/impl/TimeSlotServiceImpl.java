package com.carrefour.drive_deliver.service.impl;

import com.carrefour.drive_deliver.repository.TimeSlotRepository;
import com.carrefour.drive_deliver.service.TimeSlotService;
import com.carrefour.drive_deliver.service.dto.TimeSlotDTO;
import com.carrefour.drive_deliver.service.mapper.TimeSlotMapper;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Implementation for managing {@link com.carrefour.drive_deliver.domain.TimeSlot}.
 */
@Service
@Transactional
public class TimeSlotServiceImpl implements TimeSlotService {

    private static final Logger log = LoggerFactory.getLogger(TimeSlotServiceImpl.class);

    private final TimeSlotRepository timeSlotRepository;

    private final TimeSlotMapper timeSlotMapper;

    public TimeSlotServiceImpl(TimeSlotRepository timeSlotRepository, TimeSlotMapper timeSlotMapper) {
        this.timeSlotRepository = timeSlotRepository;
        this.timeSlotMapper = timeSlotMapper;
    }

    @Override
    public Mono<TimeSlotDTO> save(TimeSlotDTO timeSlotDTO) {
        log.debug("Request to save TimeSlot : {}", timeSlotDTO);
        return timeSlotRepository.save(timeSlotMapper.toEntity(timeSlotDTO)).map(timeSlotMapper::toDto);
    }

    @Override
    public Mono<TimeSlotDTO> update(TimeSlotDTO timeSlotDTO) {
        log.debug("Request to update TimeSlot : {}", timeSlotDTO);
        return timeSlotRepository.save(timeSlotMapper.toEntity(timeSlotDTO).setIsPersisted()).map(timeSlotMapper::toDto);
    }

    @Override
    public Mono<TimeSlotDTO> partialUpdate(TimeSlotDTO timeSlotDTO) {
        log.debug("Request to partially update TimeSlot : {}", timeSlotDTO);

        return timeSlotRepository
            .findById(timeSlotDTO.getId())
            .map(existingTimeSlot -> {
                timeSlotMapper.partialUpdate(existingTimeSlot, timeSlotDTO);

                return existingTimeSlot;
            })
            .flatMap(timeSlotRepository::save)
            .map(timeSlotMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<TimeSlotDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TimeSlots");
        return timeSlotRepository.findAllBy(pageable).map(timeSlotMapper::toDto);
    }

    public Mono<Long> countAll() {
        return timeSlotRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public Mono<TimeSlotDTO> findOne(UUID id) {
        log.debug("Request to get TimeSlot : {}", id);
        return timeSlotRepository.findById(id).map(timeSlotMapper::toDto);
    }

    @Override
    public Mono<Void> delete(UUID id) {
        log.debug("Request to delete TimeSlot : {}", id);
        return timeSlotRepository.deleteById(id);
    }
}
