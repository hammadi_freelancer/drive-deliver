package com.carrefour.drive_deliver.service.impl;

import com.carrefour.drive_deliver.repository.EvenementRepository;
import com.carrefour.drive_deliver.service.EvenementService;
import com.carrefour.drive_deliver.service.dto.EvenementDTO;
import com.carrefour.drive_deliver.service.mapper.EvenementMapper;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Implementation for managing {@link com.carrefour.drive_deliver.domain.Evenement}.
 */
@Service
@Transactional
public class EvenementServiceImpl implements EvenementService {

    private static final Logger log = LoggerFactory.getLogger(EvenementServiceImpl.class);

    private final EvenementRepository evenementRepository;

    private final EvenementMapper evenementMapper;

    public EvenementServiceImpl(EvenementRepository evenementRepository, EvenementMapper evenementMapper) {
        this.evenementRepository = evenementRepository;
        this.evenementMapper = evenementMapper;
    }

    @Override
    public Mono<EvenementDTO> save(EvenementDTO evenementDTO) {
        log.debug("Request to save Evenement : {}", evenementDTO);
        return evenementRepository.save(evenementMapper.toEntity(evenementDTO)).map(evenementMapper::toDto);
    }

    @Override
    public Mono<EvenementDTO> update(EvenementDTO evenementDTO) {
        log.debug("Request to update Evenement : {}", evenementDTO);
        return evenementRepository.save(evenementMapper.toEntity(evenementDTO).setIsPersisted()).map(evenementMapper::toDto);
    }

    @Override
    public Mono<EvenementDTO> partialUpdate(EvenementDTO evenementDTO) {
        log.debug("Request to partially update Evenement : {}", evenementDTO);

        return evenementRepository
            .findById(evenementDTO.getId())
            .map(existingEvenement -> {
                evenementMapper.partialUpdate(existingEvenement, evenementDTO);

                return existingEvenement;
            })
            .flatMap(evenementRepository::save)
            .map(evenementMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<EvenementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Evenements");
        return evenementRepository.findAllBy(pageable).map(evenementMapper::toDto);
    }

    public Mono<Long> countAll() {
        return evenementRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public Mono<EvenementDTO> findOne(UUID id) {
        log.debug("Request to get Evenement : {}", id);
        return evenementRepository.findById(id).map(evenementMapper::toDto);
    }

    @Override
    public Mono<Void> delete(UUID id) {
        log.debug("Request to delete Evenement : {}", id);
        return evenementRepository.deleteById(id);
    }
}
