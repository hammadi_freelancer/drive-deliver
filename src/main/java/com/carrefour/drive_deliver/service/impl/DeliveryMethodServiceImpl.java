package com.carrefour.drive_deliver.service.impl;

import com.carrefour.drive_deliver.repository.DeliveryMethodRepository;
import com.carrefour.drive_deliver.service.DeliveryMethodService;
import com.carrefour.drive_deliver.service.dto.DeliveryMethodDTO;
import com.carrefour.drive_deliver.service.mapper.DeliveryMethodMapper;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Implementation for managing {@link com.carrefour.drive_deliver.domain.DeliveryMethod}.
 */
@Service
@Transactional
public class DeliveryMethodServiceImpl implements DeliveryMethodService {

    private static final Logger log = LoggerFactory.getLogger(DeliveryMethodServiceImpl.class);

    private final DeliveryMethodRepository deliveryMethodRepository;

    private final DeliveryMethodMapper deliveryMethodMapper;

    public DeliveryMethodServiceImpl(DeliveryMethodRepository deliveryMethodRepository, DeliveryMethodMapper deliveryMethodMapper) {
        this.deliveryMethodRepository = deliveryMethodRepository;
        this.deliveryMethodMapper = deliveryMethodMapper;
    }

    @Override
    public Mono<DeliveryMethodDTO> save(DeliveryMethodDTO deliveryMethodDTO) {
        log.debug("Request to save DeliveryMethod : {}", deliveryMethodDTO);
        return deliveryMethodRepository.save(deliveryMethodMapper.toEntity(deliveryMethodDTO)).map(deliveryMethodMapper::toDto);
    }

    @Override
    public Mono<DeliveryMethodDTO> update(DeliveryMethodDTO deliveryMethodDTO) {
        log.debug("Request to update DeliveryMethod : {}", deliveryMethodDTO);
        return deliveryMethodRepository
            .save(deliveryMethodMapper.toEntity(deliveryMethodDTO).setIsPersisted())
            .map(deliveryMethodMapper::toDto);
    }

    @Override
    public Mono<DeliveryMethodDTO> partialUpdate(DeliveryMethodDTO deliveryMethodDTO) {
        log.debug("Request to partially update DeliveryMethod : {}", deliveryMethodDTO);

        return deliveryMethodRepository
            .findById(deliveryMethodDTO.getId())
            .map(existingDeliveryMethod -> {
                deliveryMethodMapper.partialUpdate(existingDeliveryMethod, deliveryMethodDTO);

                return existingDeliveryMethod;
            })
            .flatMap(deliveryMethodRepository::save)
            .map(deliveryMethodMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<DeliveryMethodDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DeliveryMethods");
        return deliveryMethodRepository.findAllBy(pageable).map(deliveryMethodMapper::toDto);
    }

    public Mono<Long> countAll() {
        return deliveryMethodRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public Mono<DeliveryMethodDTO> findOne(UUID id) {
        log.debug("Request to get DeliveryMethod : {}", id);
        return deliveryMethodRepository.findById(id).map(deliveryMethodMapper::toDto);
    }

    @Override
    public Mono<Void> delete(UUID id) {
        log.debug("Request to delete DeliveryMethod : {}", id);
        return deliveryMethodRepository.deleteById(id);
    }
}
