package com.carrefour.drive_deliver.service.impl;

import com.carrefour.drive_deliver.repository.DeliveryInfosRepository;
import com.carrefour.drive_deliver.service.DeliveryInfosService;
import com.carrefour.drive_deliver.service.dto.DeliveryInfosDTO;
import com.carrefour.drive_deliver.service.mapper.DeliveryInfosMapper;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Implementation for managing {@link com.carrefour.drive_deliver.domain.DeliveryInfos}.
 */
@Service
@Transactional
public class DeliveryInfosServiceImpl implements DeliveryInfosService {

    private static final Logger log = LoggerFactory.getLogger(DeliveryInfosServiceImpl.class);

    private final DeliveryInfosRepository deliveryInfosRepository;

    private final DeliveryInfosMapper deliveryInfosMapper;

    public DeliveryInfosServiceImpl(DeliveryInfosRepository deliveryInfosRepository, DeliveryInfosMapper deliveryInfosMapper) {
        this.deliveryInfosRepository = deliveryInfosRepository;
        this.deliveryInfosMapper = deliveryInfosMapper;
    }

    @Override
    public Mono<DeliveryInfosDTO> save(DeliveryInfosDTO deliveryInfosDTO) {
        log.debug("Request to save DeliveryInfos : {}", deliveryInfosDTO);
        return deliveryInfosRepository.save(deliveryInfosMapper.toEntity(deliveryInfosDTO)).map(deliveryInfosMapper::toDto);
    }

    @Override
    public Mono<DeliveryInfosDTO> update(DeliveryInfosDTO deliveryInfosDTO) {
        log.debug("Request to update DeliveryInfos : {}", deliveryInfosDTO);
        return deliveryInfosRepository
            .save(deliveryInfosMapper.toEntity(deliveryInfosDTO).setIsPersisted())
            .map(deliveryInfosMapper::toDto);
    }

    @Override
    public Mono<DeliveryInfosDTO> partialUpdate(DeliveryInfosDTO deliveryInfosDTO) {
        log.debug("Request to partially update DeliveryInfos : {}", deliveryInfosDTO);

        return deliveryInfosRepository
            .findById(deliveryInfosDTO.getId())
            .map(existingDeliveryInfos -> {
                deliveryInfosMapper.partialUpdate(existingDeliveryInfos, deliveryInfosDTO);

                return existingDeliveryInfos;
            })
            .flatMap(deliveryInfosRepository::save)
            .map(deliveryInfosMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<DeliveryInfosDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DeliveryInfos");
        return deliveryInfosRepository.findAllBy(pageable).map(deliveryInfosMapper::toDto);
    }

    /**
     *  Get all the deliveryInfos where Customer is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Flux<DeliveryInfosDTO> findAllWhereCustomerIsNull() {
        log.debug("Request to get all deliveryInfos where Customer is null");
        return deliveryInfosRepository.findAllWhereCustomerIsNull().map(deliveryInfosMapper::toDto);
    }

    public Mono<Long> countAll() {
        return deliveryInfosRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public Mono<DeliveryInfosDTO> findOne(UUID id) {
        log.debug("Request to get DeliveryInfos : {}", id);
        return deliveryInfosRepository.findById(id).map(deliveryInfosMapper::toDto);
    }

    @Override
    public Mono<Void> delete(UUID id) {
        log.debug("Request to delete DeliveryInfos : {}", id);
        return deliveryInfosRepository.deleteById(id);
    }
}
