package com.carrefour.drive_deliver.service;

import com.carrefour.drive_deliver.service.dto.DeliveryMethodDTO;
import java.util.UUID;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Interface for managing {@link com.carrefour.drive_deliver.domain.DeliveryMethod}.
 */
public interface DeliveryMethodService {
    /**
     * Save a deliveryMethod.
     *
     * @param deliveryMethodDTO the entity to save.
     * @return the persisted entity.
     */
    Mono<DeliveryMethodDTO> save(DeliveryMethodDTO deliveryMethodDTO);

    /**
     * Updates a deliveryMethod.
     *
     * @param deliveryMethodDTO the entity to update.
     * @return the persisted entity.
     */
    Mono<DeliveryMethodDTO> update(DeliveryMethodDTO deliveryMethodDTO);

    /**
     * Partially updates a deliveryMethod.
     *
     * @param deliveryMethodDTO the entity to update partially.
     * @return the persisted entity.
     */
    Mono<DeliveryMethodDTO> partialUpdate(DeliveryMethodDTO deliveryMethodDTO);

    /**
     * Get all the deliveryMethods.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Flux<DeliveryMethodDTO> findAll(Pageable pageable);

    /**
     * Returns the number of deliveryMethods available.
     * @return the number of entities in the database.
     *
     */
    Mono<Long> countAll();

    /**
     * Get the "id" deliveryMethod.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Mono<DeliveryMethodDTO> findOne(UUID id);

    /**
     * Delete the "id" deliveryMethod.
     *
     * @param id the id of the entity.
     * @return a Mono to signal the deletion
     */
    Mono<Void> delete(UUID id);
}
