package com.carrefour.drive_deliver.service;

import com.carrefour.drive_deliver.service.dto.EvenementDTO;
import java.util.UUID;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Interface for managing {@link com.carrefour.drive_deliver.domain.Evenement}.
 */
public interface EvenementService {
    /**
     * Save a evenement.
     *
     * @param evenementDTO the entity to save.
     * @return the persisted entity.
     */
    Mono<EvenementDTO> save(EvenementDTO evenementDTO);

    /**
     * Updates a evenement.
     *
     * @param evenementDTO the entity to update.
     * @return the persisted entity.
     */
    Mono<EvenementDTO> update(EvenementDTO evenementDTO);

    /**
     * Partially updates a evenement.
     *
     * @param evenementDTO the entity to update partially.
     * @return the persisted entity.
     */
    Mono<EvenementDTO> partialUpdate(EvenementDTO evenementDTO);

    /**
     * Get all the evenements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Flux<EvenementDTO> findAll(Pageable pageable);

    /**
     * Returns the number of evenements available.
     * @return the number of entities in the database.
     *
     */
    Mono<Long> countAll();

    /**
     * Get the "id" evenement.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Mono<EvenementDTO> findOne(UUID id);

    /**
     * Delete the "id" evenement.
     *
     * @param id the id of the entity.
     * @return a Mono to signal the deletion
     */
    Mono<Void> delete(UUID id);
}
