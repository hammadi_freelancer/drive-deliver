package com.carrefour.drive_deliver.service;

import com.carrefour.drive_deliver.service.dto.TimeSlotDTO;
import java.util.UUID;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Interface for managing {@link com.carrefour.drive_deliver.domain.TimeSlot}.
 */
public interface TimeSlotService {
    /**
     * Save a timeSlot.
     *
     * @param timeSlotDTO the entity to save.
     * @return the persisted entity.
     */
    Mono<TimeSlotDTO> save(TimeSlotDTO timeSlotDTO);

    /**
     * Updates a timeSlot.
     *
     * @param timeSlotDTO the entity to update.
     * @return the persisted entity.
     */
    Mono<TimeSlotDTO> update(TimeSlotDTO timeSlotDTO);

    /**
     * Partially updates a timeSlot.
     *
     * @param timeSlotDTO the entity to update partially.
     * @return the persisted entity.
     */
    Mono<TimeSlotDTO> partialUpdate(TimeSlotDTO timeSlotDTO);

    /**
     * Get all the timeSlots.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Flux<TimeSlotDTO> findAll(Pageable pageable);

    /**
     * Returns the number of timeSlots available.
     * @return the number of entities in the database.
     *
     */
    Mono<Long> countAll();

    /**
     * Get the "id" timeSlot.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Mono<TimeSlotDTO> findOne(UUID id);

    /**
     * Delete the "id" timeSlot.
     *
     * @param id the id of the entity.
     * @return a Mono to signal the deletion
     */
    Mono<Void> delete(UUID id);
}
