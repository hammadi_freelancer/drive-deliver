package com.carrefour.drive_deliver.service.mapper;

import com.carrefour.drive_deliver.domain.Evenement;
import com.carrefour.drive_deliver.service.dto.EvenementDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Evenement} and its DTO {@link EvenementDTO}.
 */
@Mapper(componentModel = "spring")
public interface EvenementMapper extends EntityMapper<EvenementDTO, Evenement> {}
