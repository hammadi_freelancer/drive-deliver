package com.carrefour.drive_deliver.service.mapper;

import com.carrefour.drive_deliver.domain.DeliveryMethod;
import com.carrefour.drive_deliver.service.dto.DeliveryMethodDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link DeliveryMethod} and its DTO {@link DeliveryMethodDTO}.
 */
@Mapper(componentModel = "spring")
public interface DeliveryMethodMapper extends EntityMapper<DeliveryMethodDTO, DeliveryMethod> {}
