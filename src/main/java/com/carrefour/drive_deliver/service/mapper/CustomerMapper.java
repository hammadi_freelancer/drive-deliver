package com.carrefour.drive_deliver.service.mapper;

import com.carrefour.drive_deliver.domain.Customer;
import com.carrefour.drive_deliver.domain.DeliveryInfos;
import com.carrefour.drive_deliver.service.dto.CustomerDTO;
import com.carrefour.drive_deliver.service.dto.DeliveryInfosDTO;
import java.util.Objects;
import java.util.UUID;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Customer} and its DTO {@link CustomerDTO}.
 */
@Mapper(componentModel = "spring")
public interface CustomerMapper extends EntityMapper<CustomerDTO, Customer> {
    @Mapping(target = "deliveryInfos", source = "deliveryInfos", qualifiedByName = "deliveryInfosId")
    CustomerDTO toDto(Customer s);

    @Named("deliveryInfosId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    DeliveryInfosDTO toDtoDeliveryInfosId(DeliveryInfos deliveryInfos);

    default String map(UUID value) {
        return Objects.toString(value, null);
    }
}
