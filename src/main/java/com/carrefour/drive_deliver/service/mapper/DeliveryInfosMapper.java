package com.carrefour.drive_deliver.service.mapper;

import com.carrefour.drive_deliver.domain.DeliveryInfos;
import com.carrefour.drive_deliver.domain.DeliveryMethod;
import com.carrefour.drive_deliver.domain.TimeSlot;
import com.carrefour.drive_deliver.service.dto.DeliveryInfosDTO;
import com.carrefour.drive_deliver.service.dto.DeliveryMethodDTO;
import com.carrefour.drive_deliver.service.dto.TimeSlotDTO;
import java.util.Objects;
import java.util.UUID;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link DeliveryInfos} and its DTO {@link DeliveryInfosDTO}.
 */
@Mapper(componentModel = "spring")
public interface DeliveryInfosMapper extends EntityMapper<DeliveryInfosDTO, DeliveryInfos> {
    @Mapping(target = "deliveryMethod", source = "deliveryMethod", qualifiedByName = "deliveryMethodId")
    @Mapping(target = "slot", source = "slot", qualifiedByName = "timeSlotId")
    DeliveryInfosDTO toDto(DeliveryInfos s);

    @Named("deliveryMethodId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    DeliveryMethodDTO toDtoDeliveryMethodId(DeliveryMethod deliveryMethod);

    @Named("timeSlotId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    TimeSlotDTO toDtoTimeSlotId(TimeSlot timeSlot);

    default String map(UUID value) {
        return Objects.toString(value, null);
    }
}
