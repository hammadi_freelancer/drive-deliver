package com.carrefour.drive_deliver.service.mapper;

import com.carrefour.drive_deliver.domain.DeliveryMethod;
import com.carrefour.drive_deliver.domain.TimeSlot;
import com.carrefour.drive_deliver.service.dto.DeliveryMethodDTO;
import com.carrefour.drive_deliver.service.dto.TimeSlotDTO;
import java.util.Objects;
import java.util.UUID;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link TimeSlot} and its DTO {@link TimeSlotDTO}.
 */
@Mapper(componentModel = "spring")
public interface TimeSlotMapper extends EntityMapper<TimeSlotDTO, TimeSlot> {
    @Mapping(target = "deliveryMethod", source = "deliveryMethod", qualifiedByName = "deliveryMethodId")
    TimeSlotDTO toDto(TimeSlot s);

    @Named("deliveryMethodId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    DeliveryMethodDTO toDtoDeliveryMethodId(DeliveryMethod deliveryMethod);

    default String map(UUID value) {
        return Objects.toString(value, null);
    }
}
