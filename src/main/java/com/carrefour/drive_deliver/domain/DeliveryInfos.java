package com.carrefour.drive_deliver.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A DeliveryInfos.
 */
@Table("delivery_infos")
@JsonIgnoreProperties(value = { "new" })
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DeliveryInfos implements Serializable, Persistable<UUID> {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private UUID id;

    @Column("delivery_date")
    private LocalDate deliveryDate;

    @Column("description")
    private String description;

    @Transient
    private boolean isPersisted;

    @Transient
    private Customer customer;

    @Transient
    @JsonIgnoreProperties(value = { "slots", "deliveryInfos" }, allowSetters = true)
    private DeliveryMethod deliveryMethod;

    @Transient
    @JsonIgnoreProperties(value = { "deliveryInfos", "deliveryMethod" }, allowSetters = true)
    private TimeSlot slot;

    @Column("delivery_method_id")
    private UUID deliveryMethodId;

    @Column("slot_id")
    private UUID slotId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public UUID getId() {
        return this.id;
    }

    public DeliveryInfos id(UUID id) {
        this.setId(id);
        return this;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public LocalDate getDeliveryDate() {
        return this.deliveryDate;
    }

    public DeliveryInfos deliveryDate(LocalDate deliveryDate) {
        this.setDeliveryDate(deliveryDate);
        return this;
    }

    public void setDeliveryDate(LocalDate deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDescription() {
        return this.description;
    }

    public DeliveryInfos description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Transient
    @Override
    public boolean isNew() {
        return !this.isPersisted;
    }

    public DeliveryInfos setIsPersisted() {
        this.isPersisted = true;
        return this;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public void setCustomer(Customer customer) {
        if (this.customer != null) {
            this.customer.setDeliveryInfos(null);
        }
        if (customer != null) {
            customer.setDeliveryInfos(this);
        }
        this.customer = customer;
    }

    public DeliveryInfos customer(Customer customer) {
        this.setCustomer(customer);
        return this;
    }

    public DeliveryMethod getDeliveryMethod() {
        return this.deliveryMethod;
    }

    public void setDeliveryMethod(DeliveryMethod deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
        this.deliveryMethodId = deliveryMethod != null ? deliveryMethod.getId() : null;
    }

    public DeliveryInfos deliveryMethod(DeliveryMethod deliveryMethod) {
        this.setDeliveryMethod(deliveryMethod);
        return this;
    }

    public TimeSlot getSlot() {
        return this.slot;
    }

    public void setSlot(TimeSlot timeSlot) {
        this.slot = timeSlot;
        this.slotId = timeSlot != null ? timeSlot.getId() : null;
    }

    public DeliveryInfos slot(TimeSlot timeSlot) {
        this.setSlot(timeSlot);
        return this;
    }

    public UUID getDeliveryMethodId() {
        return this.deliveryMethodId;
    }

    public void setDeliveryMethodId(UUID deliveryMethod) {
        this.deliveryMethodId = deliveryMethod;
    }

    public UUID getSlotId() {
        return this.slotId;
    }

    public void setSlotId(UUID timeSlot) {
        this.slotId = timeSlot;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DeliveryInfos)) {
            return false;
        }
        return getId() != null && getId().equals(((DeliveryInfos) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DeliveryInfos{" +
            "id=" + getId() +
            ", deliveryDate='" + getDeliveryDate() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
