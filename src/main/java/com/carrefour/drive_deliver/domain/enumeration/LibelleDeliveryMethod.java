package com.carrefour.drive_deliver.domain.enumeration;

/**
 * The LibelleDeliveryMethod enumeration.
 */
public enum LibelleDeliveryMethod {
    DRIVE,
    DELIVERY,
    DELIVERY_TODAY,
    DELIVERY_ASAP,
}
