package com.carrefour.drive_deliver.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A TimeSlot.
 */
@Table("time_slot")
@JsonIgnoreProperties(value = { "new" })
@SuppressWarnings("common-java:DuplicatedBlocks")
public class TimeSlot implements Serializable, Persistable<UUID> {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private UUID id;

    @Column("start_hour")
    private Instant startHour;

    @Column("end_hour")
    private Instant endHour;

    @Transient
    private boolean isPersisted;

    @Transient
    @JsonIgnoreProperties(value = { "customer", "deliveryMethod", "slot" }, allowSetters = true)
    private Set<DeliveryInfos> deliveryInfos = new HashSet<>();

    @Transient
    @JsonIgnoreProperties(value = { "slots", "deliveryInfos" }, allowSetters = true)
    private DeliveryMethod deliveryMethod;

    @Column("delivery_method_id")
    private UUID deliveryMethodId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public UUID getId() {
        return this.id;
    }

    public TimeSlot id(UUID id) {
        this.setId(id);
        return this;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Instant getStartHour() {
        return this.startHour;
    }

    public TimeSlot startHour(Instant startHour) {
        this.setStartHour(startHour);
        return this;
    }

    public void setStartHour(Instant startHour) {
        this.startHour = startHour;
    }

    public Instant getEndHour() {
        return this.endHour;
    }

    public TimeSlot endHour(Instant endHour) {
        this.setEndHour(endHour);
        return this;
    }

    public void setEndHour(Instant endHour) {
        this.endHour = endHour;
    }

    @Transient
    @Override
    public boolean isNew() {
        return !this.isPersisted;
    }

    public TimeSlot setIsPersisted() {
        this.isPersisted = true;
        return this;
    }

    public Set<DeliveryInfos> getDeliveryInfos() {
        return this.deliveryInfos;
    }

    public void setDeliveryInfos(Set<DeliveryInfos> deliveryInfos) {
        if (this.deliveryInfos != null) {
            this.deliveryInfos.forEach(i -> i.setSlot(null));
        }
        if (deliveryInfos != null) {
            deliveryInfos.forEach(i -> i.setSlot(this));
        }
        this.deliveryInfos = deliveryInfos;
    }

    public TimeSlot deliveryInfos(Set<DeliveryInfos> deliveryInfos) {
        this.setDeliveryInfos(deliveryInfos);
        return this;
    }

    public TimeSlot addDeliveryInfos(DeliveryInfos deliveryInfos) {
        this.deliveryInfos.add(deliveryInfos);
        deliveryInfos.setSlot(this);
        return this;
    }

    public TimeSlot removeDeliveryInfos(DeliveryInfos deliveryInfos) {
        this.deliveryInfos.remove(deliveryInfos);
        deliveryInfos.setSlot(null);
        return this;
    }

    public DeliveryMethod getDeliveryMethod() {
        return this.deliveryMethod;
    }

    public void setDeliveryMethod(DeliveryMethod deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
        this.deliveryMethodId = deliveryMethod != null ? deliveryMethod.getId() : null;
    }

    public TimeSlot deliveryMethod(DeliveryMethod deliveryMethod) {
        this.setDeliveryMethod(deliveryMethod);
        return this;
    }

    public UUID getDeliveryMethodId() {
        return this.deliveryMethodId;
    }

    public void setDeliveryMethodId(UUID deliveryMethod) {
        this.deliveryMethodId = deliveryMethod;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TimeSlot)) {
            return false;
        }
        return getId() != null && getId().equals(((TimeSlot) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TimeSlot{" +
            "id=" + getId() +
            ", startHour='" + getStartHour() + "'" +
            ", endHour='" + getEndHour() + "'" +
            "}";
    }
}
