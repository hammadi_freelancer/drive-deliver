package com.carrefour.drive_deliver.domain;

import org.reactivestreams.Publisher;
import org.springframework.data.r2dbc.mapping.OutboundRow;
import org.springframework.data.r2dbc.mapping.event.AfterConvertCallback;
import org.springframework.data.r2dbc.mapping.event.AfterSaveCallback;
import org.springframework.data.relational.core.sql.SqlIdentifier;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class DeliveryMethodCallback implements AfterSaveCallback<DeliveryMethod>, AfterConvertCallback<DeliveryMethod> {

    @Override
    public Publisher<DeliveryMethod> onAfterConvert(DeliveryMethod entity, SqlIdentifier table) {
        return Mono.just(entity.setIsPersisted());
    }

    @Override
    public Publisher<DeliveryMethod> onAfterSave(DeliveryMethod entity, OutboundRow outboundRow, SqlIdentifier table) {
        return Mono.just(entity.setIsPersisted());
    }
}
