package com.carrefour.drive_deliver.domain;

import com.carrefour.drive_deliver.domain.enumeration.LibelleDeliveryMethod;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A DeliveryMethod.
 */
@Table("delivery_method")
@JsonIgnoreProperties(value = { "new" })
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DeliveryMethod implements Serializable, Persistable<UUID> {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private UUID id;

    @Column("libelle")
    private LibelleDeliveryMethod libelle;

    @Column("description")
    private String description;

    @Transient
    private boolean isPersisted;

    @Transient
    @JsonIgnoreProperties(value = { "deliveryInfos", "deliveryMethod" }, allowSetters = true)
    private Set<TimeSlot> slots = new HashSet<>();

    @Transient
    @JsonIgnoreProperties(value = { "customer", "deliveryMethod", "slot" }, allowSetters = true)
    private Set<DeliveryInfos> deliveryInfos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public UUID getId() {
        return this.id;
    }

    public DeliveryMethod id(UUID id) {
        this.setId(id);
        return this;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public LibelleDeliveryMethod getLibelle() {
        return this.libelle;
    }

    public DeliveryMethod libelle(LibelleDeliveryMethod libelle) {
        this.setLibelle(libelle);
        return this;
    }

    public void setLibelle(LibelleDeliveryMethod libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return this.description;
    }

    public DeliveryMethod description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Transient
    @Override
    public boolean isNew() {
        return !this.isPersisted;
    }

    public DeliveryMethod setIsPersisted() {
        this.isPersisted = true;
        return this;
    }

    public Set<TimeSlot> getSlots() {
        return this.slots;
    }

    public void setSlots(Set<TimeSlot> timeSlots) {
        if (this.slots != null) {
            this.slots.forEach(i -> i.setDeliveryMethod(null));
        }
        if (timeSlots != null) {
            timeSlots.forEach(i -> i.setDeliveryMethod(this));
        }
        this.slots = timeSlots;
    }

    public DeliveryMethod slots(Set<TimeSlot> timeSlots) {
        this.setSlots(timeSlots);
        return this;
    }

    public DeliveryMethod addSlots(TimeSlot timeSlot) {
        this.slots.add(timeSlot);
        timeSlot.setDeliveryMethod(this);
        return this;
    }

    public DeliveryMethod removeSlots(TimeSlot timeSlot) {
        this.slots.remove(timeSlot);
        timeSlot.setDeliveryMethod(null);
        return this;
    }

    public Set<DeliveryInfos> getDeliveryInfos() {
        return this.deliveryInfos;
    }

    public void setDeliveryInfos(Set<DeliveryInfos> deliveryInfos) {
        if (this.deliveryInfos != null) {
            this.deliveryInfos.forEach(i -> i.setDeliveryMethod(null));
        }
        if (deliveryInfos != null) {
            deliveryInfos.forEach(i -> i.setDeliveryMethod(this));
        }
        this.deliveryInfos = deliveryInfos;
    }

    public DeliveryMethod deliveryInfos(Set<DeliveryInfos> deliveryInfos) {
        this.setDeliveryInfos(deliveryInfos);
        return this;
    }

    public DeliveryMethod addDeliveryInfos(DeliveryInfos deliveryInfos) {
        this.deliveryInfos.add(deliveryInfos);
        deliveryInfos.setDeliveryMethod(this);
        return this;
    }

    public DeliveryMethod removeDeliveryInfos(DeliveryInfos deliveryInfos) {
        this.deliveryInfos.remove(deliveryInfos);
        deliveryInfos.setDeliveryMethod(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DeliveryMethod)) {
            return false;
        }
        return getId() != null && getId().equals(((DeliveryMethod) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DeliveryMethod{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
