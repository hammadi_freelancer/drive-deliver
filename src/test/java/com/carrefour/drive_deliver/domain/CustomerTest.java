package com.carrefour.drive_deliver.domain;

import static com.carrefour.drive_deliver.domain.CustomerTestSamples.*;
import static com.carrefour.drive_deliver.domain.DeliveryInfosTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.carrefour.drive_deliver.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CustomerTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Customer.class);
        Customer customer1 = getCustomerSample1();
        Customer customer2 = new Customer();
        assertThat(customer1).isNotEqualTo(customer2);

        customer2.setId(customer1.getId());
        assertThat(customer1).isEqualTo(customer2);

        customer2 = getCustomerSample2();
        assertThat(customer1).isNotEqualTo(customer2);
    }

    @Test
    void deliveryInfosTest() {
        Customer customer = getCustomerRandomSampleGenerator();
        DeliveryInfos deliveryInfosBack = getDeliveryInfosRandomSampleGenerator();

        customer.setDeliveryInfos(deliveryInfosBack);
        assertThat(customer.getDeliveryInfos()).isEqualTo(deliveryInfosBack);

        customer.deliveryInfos(null);
        assertThat(customer.getDeliveryInfos()).isNull();
    }
}
