package com.carrefour.drive_deliver.domain;

import java.util.UUID;

public class DeliveryMethodTestSamples {

    public static DeliveryMethod getDeliveryMethodSample1() {
        return new DeliveryMethod().id(UUID.fromString("23d8dc04-a48b-45d9-a01d-4b728f0ad4aa")).description("description1");
    }

    public static DeliveryMethod getDeliveryMethodSample2() {
        return new DeliveryMethod().id(UUID.fromString("ad79f240-3727-46c3-b89f-2cf6ebd74367")).description("description2");
    }

    public static DeliveryMethod getDeliveryMethodRandomSampleGenerator() {
        return new DeliveryMethod().id(UUID.randomUUID()).description(UUID.randomUUID().toString());
    }
}
