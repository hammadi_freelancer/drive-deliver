package com.carrefour.drive_deliver.domain;

import static com.carrefour.drive_deliver.domain.DeliveryInfosTestSamples.*;
import static com.carrefour.drive_deliver.domain.DeliveryMethodTestSamples.*;
import static com.carrefour.drive_deliver.domain.TimeSlotTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.carrefour.drive_deliver.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class TimeSlotTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TimeSlot.class);
        TimeSlot timeSlot1 = getTimeSlotSample1();
        TimeSlot timeSlot2 = new TimeSlot();
        assertThat(timeSlot1).isNotEqualTo(timeSlot2);

        timeSlot2.setId(timeSlot1.getId());
        assertThat(timeSlot1).isEqualTo(timeSlot2);

        timeSlot2 = getTimeSlotSample2();
        assertThat(timeSlot1).isNotEqualTo(timeSlot2);
    }

    @Test
    void deliveryInfosTest() {
        TimeSlot timeSlot = getTimeSlotRandomSampleGenerator();
        DeliveryInfos deliveryInfosBack = getDeliveryInfosRandomSampleGenerator();

        timeSlot.addDeliveryInfos(deliveryInfosBack);
        assertThat(timeSlot.getDeliveryInfos()).containsOnly(deliveryInfosBack);
        assertThat(deliveryInfosBack.getSlot()).isEqualTo(timeSlot);

        timeSlot.removeDeliveryInfos(deliveryInfosBack);
        assertThat(timeSlot.getDeliveryInfos()).doesNotContain(deliveryInfosBack);
        assertThat(deliveryInfosBack.getSlot()).isNull();

        timeSlot.deliveryInfos(new HashSet<>(Set.of(deliveryInfosBack)));
        assertThat(timeSlot.getDeliveryInfos()).containsOnly(deliveryInfosBack);
        assertThat(deliveryInfosBack.getSlot()).isEqualTo(timeSlot);

        timeSlot.setDeliveryInfos(new HashSet<>());
        assertThat(timeSlot.getDeliveryInfos()).doesNotContain(deliveryInfosBack);
        assertThat(deliveryInfosBack.getSlot()).isNull();
    }

    @Test
    void deliveryMethodTest() {
        TimeSlot timeSlot = getTimeSlotRandomSampleGenerator();
        DeliveryMethod deliveryMethodBack = getDeliveryMethodRandomSampleGenerator();

        timeSlot.setDeliveryMethod(deliveryMethodBack);
        assertThat(timeSlot.getDeliveryMethod()).isEqualTo(deliveryMethodBack);

        timeSlot.deliveryMethod(null);
        assertThat(timeSlot.getDeliveryMethod()).isNull();
    }
}
