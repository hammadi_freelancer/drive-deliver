package com.carrefour.drive_deliver.domain;

import java.util.UUID;

public class CustomerTestSamples {

    public static Customer getCustomerSample1() {
        return new Customer()
            .id(UUID.fromString("23d8dc04-a48b-45d9-a01d-4b728f0ad4aa"))
            .cin("cin1")
            .nom("nom1")
            .prenom("prenom1")
            .adresse("adresse1")
            .tel("tel1")
            .email("email1");
    }

    public static Customer getCustomerSample2() {
        return new Customer()
            .id(UUID.fromString("ad79f240-3727-46c3-b89f-2cf6ebd74367"))
            .cin("cin2")
            .nom("nom2")
            .prenom("prenom2")
            .adresse("adresse2")
            .tel("tel2")
            .email("email2");
    }

    public static Customer getCustomerRandomSampleGenerator() {
        return new Customer()
            .id(UUID.randomUUID())
            .cin(UUID.randomUUID().toString())
            .nom(UUID.randomUUID().toString())
            .prenom(UUID.randomUUID().toString())
            .adresse(UUID.randomUUID().toString())
            .tel(UUID.randomUUID().toString())
            .email(UUID.randomUUID().toString());
    }
}
