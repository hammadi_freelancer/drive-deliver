package com.carrefour.drive_deliver.domain;

import java.util.UUID;

public class DeliveryInfosTestSamples {

    public static DeliveryInfos getDeliveryInfosSample1() {
        return new DeliveryInfos().id(UUID.fromString("23d8dc04-a48b-45d9-a01d-4b728f0ad4aa")).description("description1");
    }

    public static DeliveryInfos getDeliveryInfosSample2() {
        return new DeliveryInfos().id(UUID.fromString("ad79f240-3727-46c3-b89f-2cf6ebd74367")).description("description2");
    }

    public static DeliveryInfos getDeliveryInfosRandomSampleGenerator() {
        return new DeliveryInfos().id(UUID.randomUUID()).description(UUID.randomUUID().toString());
    }
}
