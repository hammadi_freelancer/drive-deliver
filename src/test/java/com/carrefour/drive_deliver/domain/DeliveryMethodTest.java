package com.carrefour.drive_deliver.domain;

import static com.carrefour.drive_deliver.domain.DeliveryInfosTestSamples.*;
import static com.carrefour.drive_deliver.domain.DeliveryMethodTestSamples.*;
import static com.carrefour.drive_deliver.domain.TimeSlotTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.carrefour.drive_deliver.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class DeliveryMethodTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DeliveryMethod.class);
        DeliveryMethod deliveryMethod1 = getDeliveryMethodSample1();
        DeliveryMethod deliveryMethod2 = new DeliveryMethod();
        assertThat(deliveryMethod1).isNotEqualTo(deliveryMethod2);

        deliveryMethod2.setId(deliveryMethod1.getId());
        assertThat(deliveryMethod1).isEqualTo(deliveryMethod2);

        deliveryMethod2 = getDeliveryMethodSample2();
        assertThat(deliveryMethod1).isNotEqualTo(deliveryMethod2);
    }

    @Test
    void slotsTest() {
        DeliveryMethod deliveryMethod = getDeliveryMethodRandomSampleGenerator();
        TimeSlot timeSlotBack = getTimeSlotRandomSampleGenerator();

        deliveryMethod.addSlots(timeSlotBack);
        assertThat(deliveryMethod.getSlots()).containsOnly(timeSlotBack);
        assertThat(timeSlotBack.getDeliveryMethod()).isEqualTo(deliveryMethod);

        deliveryMethod.removeSlots(timeSlotBack);
        assertThat(deliveryMethod.getSlots()).doesNotContain(timeSlotBack);
        assertThat(timeSlotBack.getDeliveryMethod()).isNull();

        deliveryMethod.slots(new HashSet<>(Set.of(timeSlotBack)));
        assertThat(deliveryMethod.getSlots()).containsOnly(timeSlotBack);
        assertThat(timeSlotBack.getDeliveryMethod()).isEqualTo(deliveryMethod);

        deliveryMethod.setSlots(new HashSet<>());
        assertThat(deliveryMethod.getSlots()).doesNotContain(timeSlotBack);
        assertThat(timeSlotBack.getDeliveryMethod()).isNull();
    }

    @Test
    void deliveryInfosTest() {
        DeliveryMethod deliveryMethod = getDeliveryMethodRandomSampleGenerator();
        DeliveryInfos deliveryInfosBack = getDeliveryInfosRandomSampleGenerator();

        deliveryMethod.addDeliveryInfos(deliveryInfosBack);
        assertThat(deliveryMethod.getDeliveryInfos()).containsOnly(deliveryInfosBack);
        assertThat(deliveryInfosBack.getDeliveryMethod()).isEqualTo(deliveryMethod);

        deliveryMethod.removeDeliveryInfos(deliveryInfosBack);
        assertThat(deliveryMethod.getDeliveryInfos()).doesNotContain(deliveryInfosBack);
        assertThat(deliveryInfosBack.getDeliveryMethod()).isNull();

        deliveryMethod.deliveryInfos(new HashSet<>(Set.of(deliveryInfosBack)));
        assertThat(deliveryMethod.getDeliveryInfos()).containsOnly(deliveryInfosBack);
        assertThat(deliveryInfosBack.getDeliveryMethod()).isEqualTo(deliveryMethod);

        deliveryMethod.setDeliveryInfos(new HashSet<>());
        assertThat(deliveryMethod.getDeliveryInfos()).doesNotContain(deliveryInfosBack);
        assertThat(deliveryInfosBack.getDeliveryMethod()).isNull();
    }
}
