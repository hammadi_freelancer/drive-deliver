package com.carrefour.drive_deliver.domain;

import java.util.UUID;

public class EvenementTestSamples {

    public static Evenement getEvenementSample1() {
        return new Evenement().id(UUID.fromString("23d8dc04-a48b-45d9-a01d-4b728f0ad4aa")).libelle("libelle1");
    }

    public static Evenement getEvenementSample2() {
        return new Evenement().id(UUID.fromString("ad79f240-3727-46c3-b89f-2cf6ebd74367")).libelle("libelle2");
    }

    public static Evenement getEvenementRandomSampleGenerator() {
        return new Evenement().id(UUID.randomUUID()).libelle(UUID.randomUUID().toString());
    }
}
