package com.carrefour.drive_deliver.domain;

import java.util.UUID;

public class TimeSlotTestSamples {

    public static TimeSlot getTimeSlotSample1() {
        return new TimeSlot().id(UUID.fromString("23d8dc04-a48b-45d9-a01d-4b728f0ad4aa"));
    }

    public static TimeSlot getTimeSlotSample2() {
        return new TimeSlot().id(UUID.fromString("ad79f240-3727-46c3-b89f-2cf6ebd74367"));
    }

    public static TimeSlot getTimeSlotRandomSampleGenerator() {
        return new TimeSlot().id(UUID.randomUUID());
    }
}
