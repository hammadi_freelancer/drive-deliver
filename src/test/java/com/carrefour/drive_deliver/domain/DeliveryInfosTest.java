package com.carrefour.drive_deliver.domain;

import static com.carrefour.drive_deliver.domain.CustomerTestSamples.*;
import static com.carrefour.drive_deliver.domain.DeliveryInfosTestSamples.*;
import static com.carrefour.drive_deliver.domain.DeliveryMethodTestSamples.*;
import static com.carrefour.drive_deliver.domain.TimeSlotTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.carrefour.drive_deliver.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DeliveryInfosTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DeliveryInfos.class);
        DeliveryInfos deliveryInfos1 = getDeliveryInfosSample1();
        DeliveryInfos deliveryInfos2 = new DeliveryInfos();
        assertThat(deliveryInfos1).isNotEqualTo(deliveryInfos2);

        deliveryInfos2.setId(deliveryInfos1.getId());
        assertThat(deliveryInfos1).isEqualTo(deliveryInfos2);

        deliveryInfos2 = getDeliveryInfosSample2();
        assertThat(deliveryInfos1).isNotEqualTo(deliveryInfos2);
    }

    @Test
    void customerTest() {
        DeliveryInfos deliveryInfos = getDeliveryInfosRandomSampleGenerator();
        Customer customerBack = getCustomerRandomSampleGenerator();

        deliveryInfos.setCustomer(customerBack);
        assertThat(deliveryInfos.getCustomer()).isEqualTo(customerBack);
        assertThat(customerBack.getDeliveryInfos()).isEqualTo(deliveryInfos);

        deliveryInfos.customer(null);
        assertThat(deliveryInfos.getCustomer()).isNull();
        assertThat(customerBack.getDeliveryInfos()).isNull();
    }

    @Test
    void deliveryMethodTest() {
        DeliveryInfos deliveryInfos = getDeliveryInfosRandomSampleGenerator();
        DeliveryMethod deliveryMethodBack = getDeliveryMethodRandomSampleGenerator();

        deliveryInfos.setDeliveryMethod(deliveryMethodBack);
        assertThat(deliveryInfos.getDeliveryMethod()).isEqualTo(deliveryMethodBack);

        deliveryInfos.deliveryMethod(null);
        assertThat(deliveryInfos.getDeliveryMethod()).isNull();
    }

    @Test
    void slotTest() {
        DeliveryInfos deliveryInfos = getDeliveryInfosRandomSampleGenerator();
        TimeSlot timeSlotBack = getTimeSlotRandomSampleGenerator();

        deliveryInfos.setSlot(timeSlotBack);
        assertThat(deliveryInfos.getSlot()).isEqualTo(timeSlotBack);

        deliveryInfos.slot(null);
        assertThat(deliveryInfos.getSlot()).isNull();
    }
}
