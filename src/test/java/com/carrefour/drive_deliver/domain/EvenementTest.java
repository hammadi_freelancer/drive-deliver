package com.carrefour.drive_deliver.domain;

import static com.carrefour.drive_deliver.domain.EvenementTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.carrefour.drive_deliver.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class EvenementTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Evenement.class);
        Evenement evenement1 = getEvenementSample1();
        Evenement evenement2 = new Evenement();
        assertThat(evenement1).isNotEqualTo(evenement2);

        evenement2.setId(evenement1.getId());
        assertThat(evenement1).isEqualTo(evenement2);

        evenement2 = getEvenementSample2();
        assertThat(evenement1).isNotEqualTo(evenement2);
    }
}
