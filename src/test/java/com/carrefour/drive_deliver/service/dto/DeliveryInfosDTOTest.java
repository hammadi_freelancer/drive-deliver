package com.carrefour.drive_deliver.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.carrefour.drive_deliver.web.rest.TestUtil;
import java.util.UUID;
import org.junit.jupiter.api.Test;

class DeliveryInfosDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DeliveryInfosDTO.class);
        DeliveryInfosDTO deliveryInfosDTO1 = new DeliveryInfosDTO();
        deliveryInfosDTO1.setId(UUID.randomUUID());
        DeliveryInfosDTO deliveryInfosDTO2 = new DeliveryInfosDTO();
        assertThat(deliveryInfosDTO1).isNotEqualTo(deliveryInfosDTO2);
        deliveryInfosDTO2.setId(deliveryInfosDTO1.getId());
        assertThat(deliveryInfosDTO1).isEqualTo(deliveryInfosDTO2);
        deliveryInfosDTO2.setId(UUID.randomUUID());
        assertThat(deliveryInfosDTO1).isNotEqualTo(deliveryInfosDTO2);
        deliveryInfosDTO1.setId(null);
        assertThat(deliveryInfosDTO1).isNotEqualTo(deliveryInfosDTO2);
    }
}
