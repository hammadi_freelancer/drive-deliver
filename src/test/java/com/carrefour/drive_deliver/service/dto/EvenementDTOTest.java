package com.carrefour.drive_deliver.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.carrefour.drive_deliver.web.rest.TestUtil;
import java.util.UUID;
import org.junit.jupiter.api.Test;

class EvenementDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EvenementDTO.class);
        EvenementDTO evenementDTO1 = new EvenementDTO();
        evenementDTO1.setId(UUID.randomUUID());
        EvenementDTO evenementDTO2 = new EvenementDTO();
        assertThat(evenementDTO1).isNotEqualTo(evenementDTO2);
        evenementDTO2.setId(evenementDTO1.getId());
        assertThat(evenementDTO1).isEqualTo(evenementDTO2);
        evenementDTO2.setId(UUID.randomUUID());
        assertThat(evenementDTO1).isNotEqualTo(evenementDTO2);
        evenementDTO1.setId(null);
        assertThat(evenementDTO1).isNotEqualTo(evenementDTO2);
    }
}
