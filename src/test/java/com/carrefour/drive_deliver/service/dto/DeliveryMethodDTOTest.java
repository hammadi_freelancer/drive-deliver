package com.carrefour.drive_deliver.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.carrefour.drive_deliver.web.rest.TestUtil;
import java.util.UUID;
import org.junit.jupiter.api.Test;

class DeliveryMethodDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DeliveryMethodDTO.class);
        DeliveryMethodDTO deliveryMethodDTO1 = new DeliveryMethodDTO();
        deliveryMethodDTO1.setId(UUID.randomUUID());
        DeliveryMethodDTO deliveryMethodDTO2 = new DeliveryMethodDTO();
        assertThat(deliveryMethodDTO1).isNotEqualTo(deliveryMethodDTO2);
        deliveryMethodDTO2.setId(deliveryMethodDTO1.getId());
        assertThat(deliveryMethodDTO1).isEqualTo(deliveryMethodDTO2);
        deliveryMethodDTO2.setId(UUID.randomUUID());
        assertThat(deliveryMethodDTO1).isNotEqualTo(deliveryMethodDTO2);
        deliveryMethodDTO1.setId(null);
        assertThat(deliveryMethodDTO1).isNotEqualTo(deliveryMethodDTO2);
    }
}
