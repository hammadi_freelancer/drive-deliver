package com.carrefour.drive_deliver.service.mapper;

import static com.carrefour.drive_deliver.domain.DeliveryInfosAsserts.*;
import static com.carrefour.drive_deliver.domain.DeliveryInfosTestSamples.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DeliveryInfosMapperTest {

    private DeliveryInfosMapper deliveryInfosMapper;

    @BeforeEach
    void setUp() {
        deliveryInfosMapper = new DeliveryInfosMapperImpl();
    }

    @Test
    void shouldConvertToDtoAndBack() {
        var expected = getDeliveryInfosSample1();
        var actual = deliveryInfosMapper.toEntity(deliveryInfosMapper.toDto(expected));
        assertDeliveryInfosAllPropertiesEquals(expected, actual);
    }
}
