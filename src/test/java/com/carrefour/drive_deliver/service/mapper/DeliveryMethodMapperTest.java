package com.carrefour.drive_deliver.service.mapper;

import static com.carrefour.drive_deliver.domain.DeliveryMethodAsserts.*;
import static com.carrefour.drive_deliver.domain.DeliveryMethodTestSamples.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DeliveryMethodMapperTest {

    private DeliveryMethodMapper deliveryMethodMapper;

    @BeforeEach
    void setUp() {
        deliveryMethodMapper = new DeliveryMethodMapperImpl();
    }

    @Test
    void shouldConvertToDtoAndBack() {
        var expected = getDeliveryMethodSample1();
        var actual = deliveryMethodMapper.toEntity(deliveryMethodMapper.toDto(expected));
        assertDeliveryMethodAllPropertiesEquals(expected, actual);
    }
}
