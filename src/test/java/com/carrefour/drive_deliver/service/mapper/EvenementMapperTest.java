package com.carrefour.drive_deliver.service.mapper;

import static com.carrefour.drive_deliver.domain.EvenementAsserts.*;
import static com.carrefour.drive_deliver.domain.EvenementTestSamples.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EvenementMapperTest {

    private EvenementMapper evenementMapper;

    @BeforeEach
    void setUp() {
        evenementMapper = new EvenementMapperImpl();
    }

    @Test
    void shouldConvertToDtoAndBack() {
        var expected = getEvenementSample1();
        var actual = evenementMapper.toEntity(evenementMapper.toDto(expected));
        assertEvenementAllPropertiesEquals(expected, actual);
    }
}
