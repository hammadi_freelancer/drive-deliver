package com.carrefour.drive_deliver.web.rest;

import static com.carrefour.drive_deliver.domain.DeliveryInfosAsserts.*;
import static com.carrefour.drive_deliver.web.rest.TestUtil.createUpdateProxyForBean;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;

import com.carrefour.drive_deliver.IntegrationTest;
import com.carrefour.drive_deliver.domain.DeliveryInfos;
import com.carrefour.drive_deliver.repository.DeliveryInfosRepository;
import com.carrefour.drive_deliver.repository.EntityManager;
import com.carrefour.drive_deliver.service.dto.DeliveryInfosDTO;
import com.carrefour.drive_deliver.service.mapper.DeliveryInfosMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.UUID;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link DeliveryInfosResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient(timeout = IntegrationTest.DEFAULT_ENTITY_TIMEOUT)
@WithMockUser
class DeliveryInfosResourceIT {

    private static final LocalDate DEFAULT_DELIVERY_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DELIVERY_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/delivery-infos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private ObjectMapper om;

    @Autowired
    private DeliveryInfosRepository deliveryInfosRepository;

    @Autowired
    private DeliveryInfosMapper deliveryInfosMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private DeliveryInfos deliveryInfos;

    private DeliveryInfos insertedDeliveryInfos;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DeliveryInfos createEntity(EntityManager em) {
        DeliveryInfos deliveryInfos = new DeliveryInfos()
            .id(UUID.randomUUID())
            .deliveryDate(DEFAULT_DELIVERY_DATE)
            .description(DEFAULT_DESCRIPTION);
        return deliveryInfos;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DeliveryInfos createUpdatedEntity(EntityManager em) {
        DeliveryInfos deliveryInfos = new DeliveryInfos()
            .id(UUID.randomUUID())
            .deliveryDate(UPDATED_DELIVERY_DATE)
            .description(UPDATED_DESCRIPTION);
        return deliveryInfos;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(DeliveryInfos.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @BeforeEach
    public void initTest() {
        deliveryInfos = createEntity(em);
    }

    @AfterEach
    public void cleanup() {
        if (insertedDeliveryInfos != null) {
            deliveryInfosRepository.delete(insertedDeliveryInfos).block();
            insertedDeliveryInfos = null;
        }
        deleteEntities(em);
    }

    @Test
    void createDeliveryInfos() throws Exception {
        long databaseSizeBeforeCreate = getRepositoryCount();
        deliveryInfos.setId(null);
        // Create the DeliveryInfos
        DeliveryInfosDTO deliveryInfosDTO = deliveryInfosMapper.toDto(deliveryInfos);
        var returnedDeliveryInfosDTO = webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(deliveryInfosDTO))
            .exchange()
            .expectStatus()
            .isCreated()
            .expectBody(DeliveryInfosDTO.class)
            .returnResult()
            .getResponseBody();

        // Validate the DeliveryInfos in the database
        assertIncrementedRepositoryCount(databaseSizeBeforeCreate);
        var returnedDeliveryInfos = deliveryInfosMapper.toEntity(returnedDeliveryInfosDTO);
        assertDeliveryInfosUpdatableFieldsEquals(returnedDeliveryInfos, getPersistedDeliveryInfos(returnedDeliveryInfos));

        insertedDeliveryInfos = returnedDeliveryInfos;
    }

    @Test
    void createDeliveryInfosWithExistingId() throws Exception {
        // Create the DeliveryInfos with an existing ID
        insertedDeliveryInfos = deliveryInfosRepository.save(deliveryInfos).block();
        DeliveryInfosDTO deliveryInfosDTO = deliveryInfosMapper.toDto(deliveryInfos);

        long databaseSizeBeforeCreate = getRepositoryCount();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(deliveryInfosDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the DeliveryInfos in the database
        assertSameRepositoryCount(databaseSizeBeforeCreate);
    }

    @Test
    void getAllDeliveryInfos() {
        // Initialize the database
        deliveryInfos.setId(UUID.randomUUID());
        insertedDeliveryInfos = deliveryInfosRepository.save(deliveryInfos).block();

        // Get all the deliveryInfosList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(deliveryInfos.getId().toString()))
            .jsonPath("$.[*].deliveryDate")
            .value(hasItem(DEFAULT_DELIVERY_DATE.toString()))
            .jsonPath("$.[*].description")
            .value(hasItem(DEFAULT_DESCRIPTION));
    }

    @Test
    void getDeliveryInfos() {
        // Initialize the database
        deliveryInfos.setId(UUID.randomUUID());
        insertedDeliveryInfos = deliveryInfosRepository.save(deliveryInfos).block();

        // Get the deliveryInfos
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, deliveryInfos.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(deliveryInfos.getId().toString()))
            .jsonPath("$.deliveryDate")
            .value(is(DEFAULT_DELIVERY_DATE.toString()))
            .jsonPath("$.description")
            .value(is(DEFAULT_DESCRIPTION));
    }

    @Test
    void getNonExistingDeliveryInfos() {
        // Get the deliveryInfos
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, UUID.randomUUID().toString())
            .accept(MediaType.APPLICATION_PROBLEM_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putExistingDeliveryInfos() throws Exception {
        // Initialize the database
        deliveryInfos.setId(UUID.randomUUID());
        insertedDeliveryInfos = deliveryInfosRepository.save(deliveryInfos).block();

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the deliveryInfos
        DeliveryInfos updatedDeliveryInfos = deliveryInfosRepository.findById(deliveryInfos.getId()).block();
        updatedDeliveryInfos.deliveryDate(UPDATED_DELIVERY_DATE).description(UPDATED_DESCRIPTION);
        DeliveryInfosDTO deliveryInfosDTO = deliveryInfosMapper.toDto(updatedDeliveryInfos);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, deliveryInfosDTO.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(deliveryInfosDTO))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the DeliveryInfos in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPersistedDeliveryInfosToMatchAllProperties(updatedDeliveryInfos);
    }

    @Test
    void putNonExistingDeliveryInfos() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        deliveryInfos.setId(UUID.randomUUID());

        // Create the DeliveryInfos
        DeliveryInfosDTO deliveryInfosDTO = deliveryInfosMapper.toDto(deliveryInfos);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, deliveryInfosDTO.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(deliveryInfosDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the DeliveryInfos in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchDeliveryInfos() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        deliveryInfos.setId(UUID.randomUUID());

        // Create the DeliveryInfos
        DeliveryInfosDTO deliveryInfosDTO = deliveryInfosMapper.toDto(deliveryInfos);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, UUID.randomUUID())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(deliveryInfosDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the DeliveryInfos in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamDeliveryInfos() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        deliveryInfos.setId(UUID.randomUUID());

        // Create the DeliveryInfos
        DeliveryInfosDTO deliveryInfosDTO = deliveryInfosMapper.toDto(deliveryInfos);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(deliveryInfosDTO))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the DeliveryInfos in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateDeliveryInfosWithPatch() throws Exception {
        // Initialize the database
        deliveryInfos.setId(UUID.randomUUID());
        insertedDeliveryInfos = deliveryInfosRepository.save(deliveryInfos).block();

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the deliveryInfos using partial update
        DeliveryInfos partialUpdatedDeliveryInfos = new DeliveryInfos();
        partialUpdatedDeliveryInfos.setId(deliveryInfos.getId());

        partialUpdatedDeliveryInfos.description(UPDATED_DESCRIPTION);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedDeliveryInfos.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(om.writeValueAsBytes(partialUpdatedDeliveryInfos))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the DeliveryInfos in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertDeliveryInfosUpdatableFieldsEquals(
            createUpdateProxyForBean(partialUpdatedDeliveryInfos, deliveryInfos),
            getPersistedDeliveryInfos(deliveryInfos)
        );
    }

    @Test
    void fullUpdateDeliveryInfosWithPatch() throws Exception {
        // Initialize the database
        deliveryInfos.setId(UUID.randomUUID());
        insertedDeliveryInfos = deliveryInfosRepository.save(deliveryInfos).block();

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the deliveryInfos using partial update
        DeliveryInfos partialUpdatedDeliveryInfos = new DeliveryInfos();
        partialUpdatedDeliveryInfos.setId(deliveryInfos.getId());

        partialUpdatedDeliveryInfos.deliveryDate(UPDATED_DELIVERY_DATE).description(UPDATED_DESCRIPTION);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedDeliveryInfos.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(om.writeValueAsBytes(partialUpdatedDeliveryInfos))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the DeliveryInfos in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertDeliveryInfosUpdatableFieldsEquals(partialUpdatedDeliveryInfos, getPersistedDeliveryInfos(partialUpdatedDeliveryInfos));
    }

    @Test
    void patchNonExistingDeliveryInfos() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        deliveryInfos.setId(UUID.randomUUID());

        // Create the DeliveryInfos
        DeliveryInfosDTO deliveryInfosDTO = deliveryInfosMapper.toDto(deliveryInfos);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, deliveryInfosDTO.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(om.writeValueAsBytes(deliveryInfosDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the DeliveryInfos in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchDeliveryInfos() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        deliveryInfos.setId(UUID.randomUUID());

        // Create the DeliveryInfos
        DeliveryInfosDTO deliveryInfosDTO = deliveryInfosMapper.toDto(deliveryInfos);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, UUID.randomUUID())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(om.writeValueAsBytes(deliveryInfosDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the DeliveryInfos in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamDeliveryInfos() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        deliveryInfos.setId(UUID.randomUUID());

        // Create the DeliveryInfos
        DeliveryInfosDTO deliveryInfosDTO = deliveryInfosMapper.toDto(deliveryInfos);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(om.writeValueAsBytes(deliveryInfosDTO))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the DeliveryInfos in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteDeliveryInfos() {
        // Initialize the database
        deliveryInfos.setId(UUID.randomUUID());
        insertedDeliveryInfos = deliveryInfosRepository.save(deliveryInfos).block();

        long databaseSizeBeforeDelete = getRepositoryCount();

        // Delete the deliveryInfos
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, deliveryInfos.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        assertDecrementedRepositoryCount(databaseSizeBeforeDelete);
    }

    protected long getRepositoryCount() {
        return deliveryInfosRepository.count().block();
    }

    protected void assertIncrementedRepositoryCount(long countBefore) {
        assertThat(countBefore + 1).isEqualTo(getRepositoryCount());
    }

    protected void assertDecrementedRepositoryCount(long countBefore) {
        assertThat(countBefore - 1).isEqualTo(getRepositoryCount());
    }

    protected void assertSameRepositoryCount(long countBefore) {
        assertThat(countBefore).isEqualTo(getRepositoryCount());
    }

    protected DeliveryInfos getPersistedDeliveryInfos(DeliveryInfos deliveryInfos) {
        return deliveryInfosRepository.findById(deliveryInfos.getId()).block();
    }

    protected void assertPersistedDeliveryInfosToMatchAllProperties(DeliveryInfos expectedDeliveryInfos) {
        // Test fails because reactive api returns an empty object instead of null
        // assertDeliveryInfosAllPropertiesEquals(expectedDeliveryInfos, getPersistedDeliveryInfos(expectedDeliveryInfos));
        assertDeliveryInfosUpdatableFieldsEquals(expectedDeliveryInfos, getPersistedDeliveryInfos(expectedDeliveryInfos));
    }

    protected void assertPersistedDeliveryInfosToMatchUpdatableProperties(DeliveryInfos expectedDeliveryInfos) {
        // Test fails because reactive api returns an empty object instead of null
        // assertDeliveryInfosAllUpdatablePropertiesEquals(expectedDeliveryInfos, getPersistedDeliveryInfos(expectedDeliveryInfos));
        assertDeliveryInfosUpdatableFieldsEquals(expectedDeliveryInfos, getPersistedDeliveryInfos(expectedDeliveryInfos));
    }
}
