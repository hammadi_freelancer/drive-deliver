package com.carrefour.drive_deliver.web.rest;

import static com.carrefour.drive_deliver.domain.DeliveryMethodAsserts.*;
import static com.carrefour.drive_deliver.web.rest.TestUtil.createUpdateProxyForBean;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;

import com.carrefour.drive_deliver.IntegrationTest;
import com.carrefour.drive_deliver.domain.DeliveryMethod;
import com.carrefour.drive_deliver.domain.enumeration.LibelleDeliveryMethod;
import com.carrefour.drive_deliver.repository.DeliveryMethodRepository;
import com.carrefour.drive_deliver.repository.EntityManager;
import com.carrefour.drive_deliver.service.dto.DeliveryMethodDTO;
import com.carrefour.drive_deliver.service.mapper.DeliveryMethodMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.UUID;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link DeliveryMethodResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient(timeout = IntegrationTest.DEFAULT_ENTITY_TIMEOUT)
@WithMockUser
class DeliveryMethodResourceIT {

    private static final LibelleDeliveryMethod DEFAULT_LIBELLE = LibelleDeliveryMethod.DRIVE;
    private static final LibelleDeliveryMethod UPDATED_LIBELLE = LibelleDeliveryMethod.DELIVERY;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/delivery-methods";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private ObjectMapper om;

    @Autowired
    private DeliveryMethodRepository deliveryMethodRepository;

    @Autowired
    private DeliveryMethodMapper deliveryMethodMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private DeliveryMethod deliveryMethod;

    private DeliveryMethod insertedDeliveryMethod;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DeliveryMethod createEntity(EntityManager em) {
        DeliveryMethod deliveryMethod = new DeliveryMethod()
            .id(UUID.randomUUID())
            .libelle(DEFAULT_LIBELLE)
            .description(DEFAULT_DESCRIPTION);
        return deliveryMethod;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DeliveryMethod createUpdatedEntity(EntityManager em) {
        DeliveryMethod deliveryMethod = new DeliveryMethod()
            .id(UUID.randomUUID())
            .libelle(UPDATED_LIBELLE)
            .description(UPDATED_DESCRIPTION);
        return deliveryMethod;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(DeliveryMethod.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @BeforeEach
    public void initTest() {
        deliveryMethod = createEntity(em);
    }

    @AfterEach
    public void cleanup() {
        if (insertedDeliveryMethod != null) {
            deliveryMethodRepository.delete(insertedDeliveryMethod).block();
            insertedDeliveryMethod = null;
        }
        deleteEntities(em);
    }

    @Test
    void createDeliveryMethod() throws Exception {
        long databaseSizeBeforeCreate = getRepositoryCount();
        deliveryMethod.setId(null);
        // Create the DeliveryMethod
        DeliveryMethodDTO deliveryMethodDTO = deliveryMethodMapper.toDto(deliveryMethod);
        var returnedDeliveryMethodDTO = webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(deliveryMethodDTO))
            .exchange()
            .expectStatus()
            .isCreated()
            .expectBody(DeliveryMethodDTO.class)
            .returnResult()
            .getResponseBody();

        // Validate the DeliveryMethod in the database
        assertIncrementedRepositoryCount(databaseSizeBeforeCreate);
        var returnedDeliveryMethod = deliveryMethodMapper.toEntity(returnedDeliveryMethodDTO);
        assertDeliveryMethodUpdatableFieldsEquals(returnedDeliveryMethod, getPersistedDeliveryMethod(returnedDeliveryMethod));

        insertedDeliveryMethod = returnedDeliveryMethod;
    }

    @Test
    void createDeliveryMethodWithExistingId() throws Exception {
        // Create the DeliveryMethod with an existing ID
        insertedDeliveryMethod = deliveryMethodRepository.save(deliveryMethod).block();
        DeliveryMethodDTO deliveryMethodDTO = deliveryMethodMapper.toDto(deliveryMethod);

        long databaseSizeBeforeCreate = getRepositoryCount();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(deliveryMethodDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the DeliveryMethod in the database
        assertSameRepositoryCount(databaseSizeBeforeCreate);
    }

    @Test
    void getAllDeliveryMethods() {
        // Initialize the database
        deliveryMethod.setId(UUID.randomUUID());
        insertedDeliveryMethod = deliveryMethodRepository.save(deliveryMethod).block();

        // Get all the deliveryMethodList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(deliveryMethod.getId().toString()))
            .jsonPath("$.[*].libelle")
            .value(hasItem(DEFAULT_LIBELLE.toString()))
            .jsonPath("$.[*].description")
            .value(hasItem(DEFAULT_DESCRIPTION));
    }

    @Test
    void getDeliveryMethod() {
        // Initialize the database
        deliveryMethod.setId(UUID.randomUUID());
        insertedDeliveryMethod = deliveryMethodRepository.save(deliveryMethod).block();

        // Get the deliveryMethod
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, deliveryMethod.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(deliveryMethod.getId().toString()))
            .jsonPath("$.libelle")
            .value(is(DEFAULT_LIBELLE.toString()))
            .jsonPath("$.description")
            .value(is(DEFAULT_DESCRIPTION));
    }

    @Test
    void getNonExistingDeliveryMethod() {
        // Get the deliveryMethod
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, UUID.randomUUID().toString())
            .accept(MediaType.APPLICATION_PROBLEM_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putExistingDeliveryMethod() throws Exception {
        // Initialize the database
        deliveryMethod.setId(UUID.randomUUID());
        insertedDeliveryMethod = deliveryMethodRepository.save(deliveryMethod).block();

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the deliveryMethod
        DeliveryMethod updatedDeliveryMethod = deliveryMethodRepository.findById(deliveryMethod.getId()).block();
        updatedDeliveryMethod.libelle(UPDATED_LIBELLE).description(UPDATED_DESCRIPTION);
        DeliveryMethodDTO deliveryMethodDTO = deliveryMethodMapper.toDto(updatedDeliveryMethod);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, deliveryMethodDTO.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(deliveryMethodDTO))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the DeliveryMethod in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPersistedDeliveryMethodToMatchAllProperties(updatedDeliveryMethod);
    }

    @Test
    void putNonExistingDeliveryMethod() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        deliveryMethod.setId(UUID.randomUUID());

        // Create the DeliveryMethod
        DeliveryMethodDTO deliveryMethodDTO = deliveryMethodMapper.toDto(deliveryMethod);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, deliveryMethodDTO.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(deliveryMethodDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the DeliveryMethod in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchDeliveryMethod() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        deliveryMethod.setId(UUID.randomUUID());

        // Create the DeliveryMethod
        DeliveryMethodDTO deliveryMethodDTO = deliveryMethodMapper.toDto(deliveryMethod);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, UUID.randomUUID())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(deliveryMethodDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the DeliveryMethod in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamDeliveryMethod() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        deliveryMethod.setId(UUID.randomUUID());

        // Create the DeliveryMethod
        DeliveryMethodDTO deliveryMethodDTO = deliveryMethodMapper.toDto(deliveryMethod);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(deliveryMethodDTO))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the DeliveryMethod in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateDeliveryMethodWithPatch() throws Exception {
        // Initialize the database
        deliveryMethod.setId(UUID.randomUUID());
        insertedDeliveryMethod = deliveryMethodRepository.save(deliveryMethod).block();

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the deliveryMethod using partial update
        DeliveryMethod partialUpdatedDeliveryMethod = new DeliveryMethod();
        partialUpdatedDeliveryMethod.setId(deliveryMethod.getId());

        partialUpdatedDeliveryMethod.libelle(UPDATED_LIBELLE).description(UPDATED_DESCRIPTION);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedDeliveryMethod.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(om.writeValueAsBytes(partialUpdatedDeliveryMethod))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the DeliveryMethod in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertDeliveryMethodUpdatableFieldsEquals(
            createUpdateProxyForBean(partialUpdatedDeliveryMethod, deliveryMethod),
            getPersistedDeliveryMethod(deliveryMethod)
        );
    }

    @Test
    void fullUpdateDeliveryMethodWithPatch() throws Exception {
        // Initialize the database
        deliveryMethod.setId(UUID.randomUUID());
        insertedDeliveryMethod = deliveryMethodRepository.save(deliveryMethod).block();

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the deliveryMethod using partial update
        DeliveryMethod partialUpdatedDeliveryMethod = new DeliveryMethod();
        partialUpdatedDeliveryMethod.setId(deliveryMethod.getId());

        partialUpdatedDeliveryMethod.libelle(UPDATED_LIBELLE).description(UPDATED_DESCRIPTION);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedDeliveryMethod.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(om.writeValueAsBytes(partialUpdatedDeliveryMethod))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the DeliveryMethod in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertDeliveryMethodUpdatableFieldsEquals(partialUpdatedDeliveryMethod, getPersistedDeliveryMethod(partialUpdatedDeliveryMethod));
    }

    @Test
    void patchNonExistingDeliveryMethod() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        deliveryMethod.setId(UUID.randomUUID());

        // Create the DeliveryMethod
        DeliveryMethodDTO deliveryMethodDTO = deliveryMethodMapper.toDto(deliveryMethod);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, deliveryMethodDTO.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(om.writeValueAsBytes(deliveryMethodDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the DeliveryMethod in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchDeliveryMethod() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        deliveryMethod.setId(UUID.randomUUID());

        // Create the DeliveryMethod
        DeliveryMethodDTO deliveryMethodDTO = deliveryMethodMapper.toDto(deliveryMethod);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, UUID.randomUUID())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(om.writeValueAsBytes(deliveryMethodDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the DeliveryMethod in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamDeliveryMethod() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        deliveryMethod.setId(UUID.randomUUID());

        // Create the DeliveryMethod
        DeliveryMethodDTO deliveryMethodDTO = deliveryMethodMapper.toDto(deliveryMethod);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(om.writeValueAsBytes(deliveryMethodDTO))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the DeliveryMethod in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteDeliveryMethod() {
        // Initialize the database
        deliveryMethod.setId(UUID.randomUUID());
        insertedDeliveryMethod = deliveryMethodRepository.save(deliveryMethod).block();

        long databaseSizeBeforeDelete = getRepositoryCount();

        // Delete the deliveryMethod
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, deliveryMethod.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        assertDecrementedRepositoryCount(databaseSizeBeforeDelete);
    }

    protected long getRepositoryCount() {
        return deliveryMethodRepository.count().block();
    }

    protected void assertIncrementedRepositoryCount(long countBefore) {
        assertThat(countBefore + 1).isEqualTo(getRepositoryCount());
    }

    protected void assertDecrementedRepositoryCount(long countBefore) {
        assertThat(countBefore - 1).isEqualTo(getRepositoryCount());
    }

    protected void assertSameRepositoryCount(long countBefore) {
        assertThat(countBefore).isEqualTo(getRepositoryCount());
    }

    protected DeliveryMethod getPersistedDeliveryMethod(DeliveryMethod deliveryMethod) {
        return deliveryMethodRepository.findById(deliveryMethod.getId()).block();
    }

    protected void assertPersistedDeliveryMethodToMatchAllProperties(DeliveryMethod expectedDeliveryMethod) {
        // Test fails because reactive api returns an empty object instead of null
        // assertDeliveryMethodAllPropertiesEquals(expectedDeliveryMethod, getPersistedDeliveryMethod(expectedDeliveryMethod));
        assertDeliveryMethodUpdatableFieldsEquals(expectedDeliveryMethod, getPersistedDeliveryMethod(expectedDeliveryMethod));
    }

    protected void assertPersistedDeliveryMethodToMatchUpdatableProperties(DeliveryMethod expectedDeliveryMethod) {
        // Test fails because reactive api returns an empty object instead of null
        // assertDeliveryMethodAllUpdatablePropertiesEquals(expectedDeliveryMethod, getPersistedDeliveryMethod(expectedDeliveryMethod));
        assertDeliveryMethodUpdatableFieldsEquals(expectedDeliveryMethod, getPersistedDeliveryMethod(expectedDeliveryMethod));
    }
}
