package com.carrefour.drive_deliver.web.rest;

import static com.carrefour.drive_deliver.domain.TimeSlotAsserts.*;
import static com.carrefour.drive_deliver.web.rest.TestUtil.createUpdateProxyForBean;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;

import com.carrefour.drive_deliver.IntegrationTest;
import com.carrefour.drive_deliver.domain.TimeSlot;
import com.carrefour.drive_deliver.repository.EntityManager;
import com.carrefour.drive_deliver.repository.TimeSlotRepository;
import com.carrefour.drive_deliver.service.dto.TimeSlotDTO;
import com.carrefour.drive_deliver.service.mapper.TimeSlotMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.UUID;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link TimeSlotResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient(timeout = IntegrationTest.DEFAULT_ENTITY_TIMEOUT)
@WithMockUser
class TimeSlotResourceIT {

    private static final Instant DEFAULT_START_HOUR = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_START_HOUR = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_END_HOUR = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_END_HOUR = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/time-slots";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private ObjectMapper om;

    @Autowired
    private TimeSlotRepository timeSlotRepository;

    @Autowired
    private TimeSlotMapper timeSlotMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private TimeSlot timeSlot;

    private TimeSlot insertedTimeSlot;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TimeSlot createEntity(EntityManager em) {
        TimeSlot timeSlot = new TimeSlot().id(UUID.randomUUID()).startHour(DEFAULT_START_HOUR).endHour(DEFAULT_END_HOUR);
        return timeSlot;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TimeSlot createUpdatedEntity(EntityManager em) {
        TimeSlot timeSlot = new TimeSlot().id(UUID.randomUUID()).startHour(UPDATED_START_HOUR).endHour(UPDATED_END_HOUR);
        return timeSlot;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(TimeSlot.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @BeforeEach
    public void initTest() {
        timeSlot = createEntity(em);
    }

    @AfterEach
    public void cleanup() {
        if (insertedTimeSlot != null) {
            timeSlotRepository.delete(insertedTimeSlot).block();
            insertedTimeSlot = null;
        }
        deleteEntities(em);
    }

    @Test
    void createTimeSlot() throws Exception {
        long databaseSizeBeforeCreate = getRepositoryCount();
        timeSlot.setId(null);
        // Create the TimeSlot
        TimeSlotDTO timeSlotDTO = timeSlotMapper.toDto(timeSlot);
        var returnedTimeSlotDTO = webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(timeSlotDTO))
            .exchange()
            .expectStatus()
            .isCreated()
            .expectBody(TimeSlotDTO.class)
            .returnResult()
            .getResponseBody();

        // Validate the TimeSlot in the database
        assertIncrementedRepositoryCount(databaseSizeBeforeCreate);
        var returnedTimeSlot = timeSlotMapper.toEntity(returnedTimeSlotDTO);
        assertTimeSlotUpdatableFieldsEquals(returnedTimeSlot, getPersistedTimeSlot(returnedTimeSlot));

        insertedTimeSlot = returnedTimeSlot;
    }

    @Test
    void createTimeSlotWithExistingId() throws Exception {
        // Create the TimeSlot with an existing ID
        insertedTimeSlot = timeSlotRepository.save(timeSlot).block();
        TimeSlotDTO timeSlotDTO = timeSlotMapper.toDto(timeSlot);

        long databaseSizeBeforeCreate = getRepositoryCount();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(timeSlotDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the TimeSlot in the database
        assertSameRepositoryCount(databaseSizeBeforeCreate);
    }

    @Test
    void getAllTimeSlots() {
        // Initialize the database
        timeSlot.setId(UUID.randomUUID());
        insertedTimeSlot = timeSlotRepository.save(timeSlot).block();

        // Get all the timeSlotList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(timeSlot.getId().toString()))
            .jsonPath("$.[*].startHour")
            .value(hasItem(DEFAULT_START_HOUR.toString()))
            .jsonPath("$.[*].endHour")
            .value(hasItem(DEFAULT_END_HOUR.toString()));
    }

    @Test
    void getTimeSlot() {
        // Initialize the database
        timeSlot.setId(UUID.randomUUID());
        insertedTimeSlot = timeSlotRepository.save(timeSlot).block();

        // Get the timeSlot
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, timeSlot.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(timeSlot.getId().toString()))
            .jsonPath("$.startHour")
            .value(is(DEFAULT_START_HOUR.toString()))
            .jsonPath("$.endHour")
            .value(is(DEFAULT_END_HOUR.toString()));
    }

    @Test
    void getNonExistingTimeSlot() {
        // Get the timeSlot
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, UUID.randomUUID().toString())
            .accept(MediaType.APPLICATION_PROBLEM_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putExistingTimeSlot() throws Exception {
        // Initialize the database
        timeSlot.setId(UUID.randomUUID());
        insertedTimeSlot = timeSlotRepository.save(timeSlot).block();

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the timeSlot
        TimeSlot updatedTimeSlot = timeSlotRepository.findById(timeSlot.getId()).block();
        updatedTimeSlot.startHour(UPDATED_START_HOUR).endHour(UPDATED_END_HOUR);
        TimeSlotDTO timeSlotDTO = timeSlotMapper.toDto(updatedTimeSlot);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, timeSlotDTO.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(timeSlotDTO))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the TimeSlot in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPersistedTimeSlotToMatchAllProperties(updatedTimeSlot);
    }

    @Test
    void putNonExistingTimeSlot() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        timeSlot.setId(UUID.randomUUID());

        // Create the TimeSlot
        TimeSlotDTO timeSlotDTO = timeSlotMapper.toDto(timeSlot);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, timeSlotDTO.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(timeSlotDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the TimeSlot in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchTimeSlot() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        timeSlot.setId(UUID.randomUUID());

        // Create the TimeSlot
        TimeSlotDTO timeSlotDTO = timeSlotMapper.toDto(timeSlot);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, UUID.randomUUID())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(timeSlotDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the TimeSlot in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamTimeSlot() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        timeSlot.setId(UUID.randomUUID());

        // Create the TimeSlot
        TimeSlotDTO timeSlotDTO = timeSlotMapper.toDto(timeSlot);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(timeSlotDTO))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the TimeSlot in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateTimeSlotWithPatch() throws Exception {
        // Initialize the database
        timeSlot.setId(UUID.randomUUID());
        insertedTimeSlot = timeSlotRepository.save(timeSlot).block();

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the timeSlot using partial update
        TimeSlot partialUpdatedTimeSlot = new TimeSlot();
        partialUpdatedTimeSlot.setId(timeSlot.getId());

        partialUpdatedTimeSlot.startHour(UPDATED_START_HOUR).endHour(UPDATED_END_HOUR);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedTimeSlot.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(om.writeValueAsBytes(partialUpdatedTimeSlot))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the TimeSlot in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertTimeSlotUpdatableFieldsEquals(createUpdateProxyForBean(partialUpdatedTimeSlot, timeSlot), getPersistedTimeSlot(timeSlot));
    }

    @Test
    void fullUpdateTimeSlotWithPatch() throws Exception {
        // Initialize the database
        timeSlot.setId(UUID.randomUUID());
        insertedTimeSlot = timeSlotRepository.save(timeSlot).block();

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the timeSlot using partial update
        TimeSlot partialUpdatedTimeSlot = new TimeSlot();
        partialUpdatedTimeSlot.setId(timeSlot.getId());

        partialUpdatedTimeSlot.startHour(UPDATED_START_HOUR).endHour(UPDATED_END_HOUR);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedTimeSlot.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(om.writeValueAsBytes(partialUpdatedTimeSlot))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the TimeSlot in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertTimeSlotUpdatableFieldsEquals(partialUpdatedTimeSlot, getPersistedTimeSlot(partialUpdatedTimeSlot));
    }

    @Test
    void patchNonExistingTimeSlot() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        timeSlot.setId(UUID.randomUUID());

        // Create the TimeSlot
        TimeSlotDTO timeSlotDTO = timeSlotMapper.toDto(timeSlot);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, timeSlotDTO.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(om.writeValueAsBytes(timeSlotDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the TimeSlot in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchTimeSlot() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        timeSlot.setId(UUID.randomUUID());

        // Create the TimeSlot
        TimeSlotDTO timeSlotDTO = timeSlotMapper.toDto(timeSlot);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, UUID.randomUUID())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(om.writeValueAsBytes(timeSlotDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the TimeSlot in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamTimeSlot() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        timeSlot.setId(UUID.randomUUID());

        // Create the TimeSlot
        TimeSlotDTO timeSlotDTO = timeSlotMapper.toDto(timeSlot);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(om.writeValueAsBytes(timeSlotDTO))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the TimeSlot in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteTimeSlot() {
        // Initialize the database
        timeSlot.setId(UUID.randomUUID());
        insertedTimeSlot = timeSlotRepository.save(timeSlot).block();

        long databaseSizeBeforeDelete = getRepositoryCount();

        // Delete the timeSlot
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, timeSlot.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        assertDecrementedRepositoryCount(databaseSizeBeforeDelete);
    }

    protected long getRepositoryCount() {
        return timeSlotRepository.count().block();
    }

    protected void assertIncrementedRepositoryCount(long countBefore) {
        assertThat(countBefore + 1).isEqualTo(getRepositoryCount());
    }

    protected void assertDecrementedRepositoryCount(long countBefore) {
        assertThat(countBefore - 1).isEqualTo(getRepositoryCount());
    }

    protected void assertSameRepositoryCount(long countBefore) {
        assertThat(countBefore).isEqualTo(getRepositoryCount());
    }

    protected TimeSlot getPersistedTimeSlot(TimeSlot timeSlot) {
        return timeSlotRepository.findById(timeSlot.getId()).block();
    }

    protected void assertPersistedTimeSlotToMatchAllProperties(TimeSlot expectedTimeSlot) {
        // Test fails because reactive api returns an empty object instead of null
        // assertTimeSlotAllPropertiesEquals(expectedTimeSlot, getPersistedTimeSlot(expectedTimeSlot));
        assertTimeSlotUpdatableFieldsEquals(expectedTimeSlot, getPersistedTimeSlot(expectedTimeSlot));
    }

    protected void assertPersistedTimeSlotToMatchUpdatableProperties(TimeSlot expectedTimeSlot) {
        // Test fails because reactive api returns an empty object instead of null
        // assertTimeSlotAllUpdatablePropertiesEquals(expectedTimeSlot, getPersistedTimeSlot(expectedTimeSlot));
        assertTimeSlotUpdatableFieldsEquals(expectedTimeSlot, getPersistedTimeSlot(expectedTimeSlot));
    }
}
