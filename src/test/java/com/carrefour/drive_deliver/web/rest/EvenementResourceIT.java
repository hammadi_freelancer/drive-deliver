package com.carrefour.drive_deliver.web.rest;

import static com.carrefour.drive_deliver.domain.EvenementAsserts.*;
import static com.carrefour.drive_deliver.web.rest.TestUtil.createUpdateProxyForBean;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;

import com.carrefour.drive_deliver.IntegrationTest;
import com.carrefour.drive_deliver.domain.Evenement;
import com.carrefour.drive_deliver.repository.EntityManager;
import com.carrefour.drive_deliver.repository.EvenementRepository;
import com.carrefour.drive_deliver.service.dto.EvenementDTO;
import com.carrefour.drive_deliver.service.mapper.EvenementMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.UUID;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link EvenementResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient(timeout = IntegrationTest.DEFAULT_ENTITY_TIMEOUT)
@WithMockUser
class EvenementResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/evenements";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private ObjectMapper om;

    @Autowired
    private EvenementRepository evenementRepository;

    @Autowired
    private EvenementMapper evenementMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Evenement evenement;

    private Evenement insertedEvenement;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Evenement createEntity(EntityManager em) {
        Evenement evenement = new Evenement().id(UUID.randomUUID()).libelle(DEFAULT_LIBELLE);
        return evenement;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Evenement createUpdatedEntity(EntityManager em) {
        Evenement evenement = new Evenement().id(UUID.randomUUID()).libelle(UPDATED_LIBELLE);
        return evenement;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Evenement.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @BeforeEach
    public void initTest() {
        evenement = createEntity(em);
    }

    @AfterEach
    public void cleanup() {
        if (insertedEvenement != null) {
            evenementRepository.delete(insertedEvenement).block();
            insertedEvenement = null;
        }
        deleteEntities(em);
    }

    @Test
    void createEvenement() throws Exception {
        long databaseSizeBeforeCreate = getRepositoryCount();
        evenement.setId(null);
        // Create the Evenement
        EvenementDTO evenementDTO = evenementMapper.toDto(evenement);
        var returnedEvenementDTO = webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(evenementDTO))
            .exchange()
            .expectStatus()
            .isCreated()
            .expectBody(EvenementDTO.class)
            .returnResult()
            .getResponseBody();

        // Validate the Evenement in the database
        assertIncrementedRepositoryCount(databaseSizeBeforeCreate);
        var returnedEvenement = evenementMapper.toEntity(returnedEvenementDTO);
        assertEvenementUpdatableFieldsEquals(returnedEvenement, getPersistedEvenement(returnedEvenement));

        insertedEvenement = returnedEvenement;
    }

    @Test
    void createEvenementWithExistingId() throws Exception {
        // Create the Evenement with an existing ID
        insertedEvenement = evenementRepository.save(evenement).block();
        EvenementDTO evenementDTO = evenementMapper.toDto(evenement);

        long databaseSizeBeforeCreate = getRepositoryCount();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(evenementDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Evenement in the database
        assertSameRepositoryCount(databaseSizeBeforeCreate);
    }

    @Test
    void getAllEvenements() {
        // Initialize the database
        evenement.setId(UUID.randomUUID());
        insertedEvenement = evenementRepository.save(evenement).block();

        // Get all the evenementList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(evenement.getId().toString()))
            .jsonPath("$.[*].libelle")
            .value(hasItem(DEFAULT_LIBELLE));
    }

    @Test
    void getEvenement() {
        // Initialize the database
        evenement.setId(UUID.randomUUID());
        insertedEvenement = evenementRepository.save(evenement).block();

        // Get the evenement
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, evenement.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(evenement.getId().toString()))
            .jsonPath("$.libelle")
            .value(is(DEFAULT_LIBELLE));
    }

    @Test
    void getNonExistingEvenement() {
        // Get the evenement
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, UUID.randomUUID().toString())
            .accept(MediaType.APPLICATION_PROBLEM_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putExistingEvenement() throws Exception {
        // Initialize the database
        evenement.setId(UUID.randomUUID());
        insertedEvenement = evenementRepository.save(evenement).block();

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the evenement
        Evenement updatedEvenement = evenementRepository.findById(evenement.getId()).block();
        updatedEvenement.libelle(UPDATED_LIBELLE);
        EvenementDTO evenementDTO = evenementMapper.toDto(updatedEvenement);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, evenementDTO.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(evenementDTO))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Evenement in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPersistedEvenementToMatchAllProperties(updatedEvenement);
    }

    @Test
    void putNonExistingEvenement() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        evenement.setId(UUID.randomUUID());

        // Create the Evenement
        EvenementDTO evenementDTO = evenementMapper.toDto(evenement);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, evenementDTO.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(evenementDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Evenement in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchEvenement() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        evenement.setId(UUID.randomUUID());

        // Create the Evenement
        EvenementDTO evenementDTO = evenementMapper.toDto(evenement);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, UUID.randomUUID())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(evenementDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Evenement in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamEvenement() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        evenement.setId(UUID.randomUUID());

        // Create the Evenement
        EvenementDTO evenementDTO = evenementMapper.toDto(evenement);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(om.writeValueAsBytes(evenementDTO))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Evenement in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateEvenementWithPatch() throws Exception {
        // Initialize the database
        evenement.setId(UUID.randomUUID());
        insertedEvenement = evenementRepository.save(evenement).block();

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the evenement using partial update
        Evenement partialUpdatedEvenement = new Evenement();
        partialUpdatedEvenement.setId(evenement.getId());

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedEvenement.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(om.writeValueAsBytes(partialUpdatedEvenement))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Evenement in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertEvenementUpdatableFieldsEquals(
            createUpdateProxyForBean(partialUpdatedEvenement, evenement),
            getPersistedEvenement(evenement)
        );
    }

    @Test
    void fullUpdateEvenementWithPatch() throws Exception {
        // Initialize the database
        evenement.setId(UUID.randomUUID());
        insertedEvenement = evenementRepository.save(evenement).block();

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the evenement using partial update
        Evenement partialUpdatedEvenement = new Evenement();
        partialUpdatedEvenement.setId(evenement.getId());

        partialUpdatedEvenement.libelle(UPDATED_LIBELLE);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedEvenement.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(om.writeValueAsBytes(partialUpdatedEvenement))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Evenement in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertEvenementUpdatableFieldsEquals(partialUpdatedEvenement, getPersistedEvenement(partialUpdatedEvenement));
    }

    @Test
    void patchNonExistingEvenement() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        evenement.setId(UUID.randomUUID());

        // Create the Evenement
        EvenementDTO evenementDTO = evenementMapper.toDto(evenement);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, evenementDTO.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(om.writeValueAsBytes(evenementDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Evenement in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchEvenement() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        evenement.setId(UUID.randomUUID());

        // Create the Evenement
        EvenementDTO evenementDTO = evenementMapper.toDto(evenement);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, UUID.randomUUID())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(om.writeValueAsBytes(evenementDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Evenement in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamEvenement() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        evenement.setId(UUID.randomUUID());

        // Create the Evenement
        EvenementDTO evenementDTO = evenementMapper.toDto(evenement);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(om.writeValueAsBytes(evenementDTO))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Evenement in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteEvenement() {
        // Initialize the database
        evenement.setId(UUID.randomUUID());
        insertedEvenement = evenementRepository.save(evenement).block();

        long databaseSizeBeforeDelete = getRepositoryCount();

        // Delete the evenement
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, evenement.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        assertDecrementedRepositoryCount(databaseSizeBeforeDelete);
    }

    protected long getRepositoryCount() {
        return evenementRepository.count().block();
    }

    protected void assertIncrementedRepositoryCount(long countBefore) {
        assertThat(countBefore + 1).isEqualTo(getRepositoryCount());
    }

    protected void assertDecrementedRepositoryCount(long countBefore) {
        assertThat(countBefore - 1).isEqualTo(getRepositoryCount());
    }

    protected void assertSameRepositoryCount(long countBefore) {
        assertThat(countBefore).isEqualTo(getRepositoryCount());
    }

    protected Evenement getPersistedEvenement(Evenement evenement) {
        return evenementRepository.findById(evenement.getId()).block();
    }

    protected void assertPersistedEvenementToMatchAllProperties(Evenement expectedEvenement) {
        // Test fails because reactive api returns an empty object instead of null
        // assertEvenementAllPropertiesEquals(expectedEvenement, getPersistedEvenement(expectedEvenement));
        assertEvenementUpdatableFieldsEquals(expectedEvenement, getPersistedEvenement(expectedEvenement));
    }

    protected void assertPersistedEvenementToMatchUpdatableProperties(Evenement expectedEvenement) {
        // Test fails because reactive api returns an empty object instead of null
        // assertEvenementAllUpdatablePropertiesEquals(expectedEvenement, getPersistedEvenement(expectedEvenement));
        assertEvenementUpdatableFieldsEquals(expectedEvenement, getPersistedEvenement(expectedEvenement));
    }
}
